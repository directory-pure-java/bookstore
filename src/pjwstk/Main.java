package pjwstk;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.security.sasl.AuthorizeCallback;

import pjwstk.data.AddressBookshelf;
import pjwstk.data.AddressCell;
import pjwstk.data.CompletationStatus;
import pjwstk.data.ContainerStatus;
import pjwstk.data.EmployeePosition;
import pjwstk.data.EmployeeStatus;
import pjwstk.data.ObjectPlus;
import pjwstk.data.OrderStatus;
import pjwstk.data.ScannerStatus;
import pjwstk.entity.Author;
import pjwstk.entity.Book;
import pjwstk.entity.Bookshelf;
import pjwstk.entity.Bookshelf.Cell;
import pjwstk.entity.actors.Customer;
import pjwstk.entity.actors.Employee;
import pjwstk.entity.actors.Manager;
import pjwstk.entity.actors.Packer;
import pjwstk.entity.actors.Position;
import pjwstk.entity.actors.Storekeeper;
import pjwstk.entity.actors.User;
import pjwstk.entity.Container;
import pjwstk.entity.Order;
import pjwstk.entity.OrderDetails;
import pjwstk.entity.Person;
import pjwstk.entity.Scanner;

public class Main {

	public static void main(String[] args) throws Exception {

		// wsad
//		ObjectPlus.writeExtents();
//		ObjectPlus.readExtents();
//		Manager manager = new Manager("01", "Alfred", "Pennyworth", Arrays.asList("500400300"));
//		manager.createNewBookshelf(new AddressBookshelf(2, 1, 10));
//		manager.createNewBookshelf(new AddressBookshelf(0, 0, 0));
//		manager.createNewCell(new AddressBookshelf(2, 1, 10), new AddressCell(20, 20));
//		manager.createNewCell(new AddressBookshelf(2, 1, 10), new AddressCell(5, 14));
//		manager.createNewCell(new AddressBookshelf(0, 0, 0), new AddressCell(1, 1));
//		manager.createNewCell(new AddressBookshelf(0, 0, 0), new AddressCell(1, 2));
//		manager.createNewAuthor("Marcel", "Proust", 1871);
//		manager.createNewAuthor("Adam", "Mickiewicz", 1798);
//		manager.createNewBook("9789626347539", "In Search of Lost Time", 20, "Marcel", "Proust");
//		manager.createNewBook("9780781800334", "Pan Tadeusz", 10, "Adam", "Mickiewicz");
//		manager.setBookInCell("9789626347539", new AddressBookshelf(2, 1, 10), new AddressCell(20, 20));
//		manager.setBookInCell("9780781800334", new AddressBookshelf(2, 1, 10), new AddressCell(5, 14));
//		
//		manager.addBooksInCell("9789626347539", 10);
//		manager.addBooksInCell("9780781800334", 10);
//		manager.createNewCustomer("jan", "Kowalski", "jk@wp.pl");
//		Customer customer = Customer.find("jk@wp.pl").get();
//		customer.makeOrder();
//		customer.createOrderDetails("9789626347539", 4);
//		customer.createOrderDetails("9780781800334", 2);
//		manager.createNewEmployee("02", " Tissai ", " Vries", Arrays.asList("606799800"));
//		manager.createNewScanner(12);
//		manager.createNewContainer(20);
//		manager.setJobPosition("02", EmployeePosition.PACKER);
//		ObjectPlus.writeExtents();

		
		
		ObjectPlus.readExtents();
		ObjectPlus.showExtent(Container.class);
		ObjectPlus.showExtent(Scanner.class);
		System.out.println();
		ObjectPlus.showExtent(Packer.class);
		System.out.println(); 
		ObjectPlus.showExtent(Order.class);
		ObjectPlus.showExtent(OrderDetails.class);
		System.out.println();
		ObjectPlus.showExtent(Book.class);
	

	}

}
