package pjwstk.interfaces;

import java.io.IOException;

import pjwstk.data.AddressBookshelf;
import pjwstk.data.AddressCell;


public interface IStorekeeper {

	abstract void createNewAuthor(String name, String surname, int bYear) throws Exception;

	abstract void createNewBook(String ISBN, String title, int stock, String name, String surname) throws Exception;

	abstract public void createNewBookshelf(AddressBookshelf addressBookshelf) throws Exception;

	abstract public void createNewCell(AddressBookshelf booAddressBookshelf, AddressCell addressCell) throws Exception;

	abstract public void setBookInCell(String ISBN, AddressBookshelf ab, AddressCell ac)
			throws ClassNotFoundException, IOException;

	abstract void addBooksInCell(String ISBN, int quantity) throws ClassNotFoundException, IOException;

}