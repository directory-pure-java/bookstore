package pjwstk.gui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.EmptyBorder;

import pjwstk.data.Service;

import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.SystemColor;
import javax.swing.SwingConstants;

public class CompletingOrderFrame1 extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Service service;

	private JPanel contentPane;
	private JMenuBar menuBar;
	private JMenu mnNewMenu;
	private JMenu mnCart;
	private JLabel lblNewLabel_1;

	private JLabel lblNewLabel_3;
	private JProgressBar progressBar;
	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CompletingOrderFrame1 frame = new CompletingOrderFrame1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public CompletingOrderFrame1() throws HeadlessException {
		super();

		init();
		setTitle("Completing #1");
	}

	/**
	 * Create the frame.
	 * 
	 * @param tmpContainer
	 * @param tmpOrderDetails
	 * @param tmpPacker2
	 */

	public CompletingOrderFrame1(Service service) {
		this.service = service;
		this.frame = this;
		service.getCountCompletedOrderDetails();

		init();
		setTitle("Completing #1");
		progressBar.setValue(service.getCountCompletedOrderDetails());

		mnCart.setText(String.valueOf(service.getTmpContainer().getId()));
		mnNewMenu.setText(service.getTmpPacker().getId());

		lblNewLabel_3.setText(""
				+ service.getCurrentPositionOrderDetails().get().getBook().getCell().getAddressBookshelf().getFloor()
				+ "-"
				+ service.getCurrentPositionOrderDetails().get().getBook().getCell().getAddressBookshelf().getRow());
	}

	public void init() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 265, 382);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(230, 230, 250));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		menuBar = new JMenuBar();
		menuBar.setBackground(Color.WHITE);
		menuBar.setBounds(0, 0, 248, 21);

		JMenu mnNewMenu_1 = new JMenu("Menu");
		menuBar.add(mnNewMenu_1);

		JMenuItem mntmOrderDetails = new JMenuItem("Order");
		mntmOrderDetails.setFont(new Font("Segoe UI", Font.PLAIN, 11));
		mntmOrderDetails.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {
					GetOrderDetailsFrame godf = new GetOrderDetailsFrame(service, frame);
					godf.setVisible(true);

				} catch (Exception e) {

					e.printStackTrace();
				}

			}
		});

		mnNewMenu_1.add(mntmOrderDetails);

		JMenuItem mntmMenu = new JMenuItem("Logout");
		mntmMenu.setEnabled(false);
		mntmMenu.setFont(new Font("Segoe UI Emoji", Font.PLAIN, 11));
		mntmMenu.setHorizontalAlignment(SwingConstants.LEFT);
		mntmMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {
					service.logout();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				dispose();

			}
		});
		mnNewMenu_1.add(mntmMenu);
		mnNewMenu_1.add(mntmMenu);
		menuBar.add(Box.createHorizontalGlue());
		contentPane.add(menuBar);

		menuBar.add(Box.createHorizontalGlue());
		contentPane.add(menuBar);

		mnCart = new JMenu("");
		mnCart.setEnabled(false);
		mnCart.setForeground(SystemColor.controlText);
		mnCart.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\cart.png"));
		menuBar.add(mnCart);

		mnNewMenu = new JMenu("");
		mnNewMenu.setEnabled(false);
		mnNewMenu.setBackground(Color.GRAY);
		mnNewMenu.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\user.png"));
		menuBar.add(mnNewMenu);

		progressBar = new JProgressBar(0, service.getTmpOrderDetails().size());
		progressBar.setForeground(new Color(0, 0, 128));
		progressBar.setBounds(10, 329, 229, 5);
		contentPane.add(progressBar);

		JLabel lblNewLabel = new JLabel("Bookshelf row code:");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setForeground(new Color(0, 0, 128));
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel.setBounds(10, 83, 230, 40);
		contentPane.add(lblNewLabel);

		lblNewLabel_3 = new JLabel("", JLabel.CENTER);
		lblNewLabel_3.setForeground(new Color(0, 0, 128));
		lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 30));
		lblNewLabel_3.setBounds(10, 121, 229, 38);
		contentPane.add(lblNewLabel_3);

		textField = new JTextField();
		textField.setHorizontalAlignment(JTextField.CENTER);
		textField.setForeground(new Color(0, 0, 128));
		textField.setText("");
		textField.setFont(new Font("Tahoma", Font.BOLD, 26));
		textField.setColumns(5);
		textField.setBounds(10, 184, 229, 38);
		contentPane.add(textField);

		JButton button = new JButton("ENTER");
		button.setFocusable(false);
		button.setForeground(new Color(255, 255, 255));
		button.setFont(new Font("Tahoma", Font.BOLD, 14));

		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel_1.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\circlegreen.png"));
				lblNewLabel_1.setText("");

				if (lblNewLabel_3.getText().equals(textField.getText())) {

					CompletingOrderFrame2 cof2 = new CompletingOrderFrame2(service);
					cof2.setVisible(true);
					dispose();

				} else {

					lblNewLabel_1.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\circlered.png"));
					lblNewLabel_1.setText("Invalid entered number.");

				}

			}
		});
		button.setBackground(new Color(25, 25, 112));
		button.setBounds(10, 250, 229, 63);
		contentPane.add(button);

		lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblNewLabel_1.setForeground(Color.RED);
		lblNewLabel_1.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\circlegreen.png"));
		lblNewLabel_1.setBounds(10, 21, 237, 14);
		contentPane.add(lblNewLabel_1);

	}

//	Timer timer = new Timer(1000, this);

	private JTextField textField;

//	@Override
//	public void actionPerformed(ActionEvent e) {
//
//		progressBar.setValue(1);
//
//	}

//	Timer timer = new Timer(1000, this);
//
//	int i;
//
//	JProgressBar progressBar = new JProgressBar(0, 20);

//	i++;
//	progressBar.setValue(i);
}
//	Timer timer = new Timer(1000, this);
//
//	int i;
//
//	JProgressBar progressBar = new JProgressBar(0, 20);
//	private JTextField textField;
//	private JPasswordField passwordField;
