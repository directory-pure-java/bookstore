package pjwstk.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Optional;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import pjwstk.data.Alert;
import pjwstk.data.CompletationStatus;
import pjwstk.data.Service;
import pjwstk.entity.OrderDetails;

public class CompletingOrderFrame5 extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JPanel contentPane;

	private JMenuBar menuBar;
	private JMenu mnNewMenu;
	private JMenu mnCart;
	private JLabel lblNewLabel_1;

	private JLabel label;
	private JFrame frame;
	private JProgressBar progressBar;

	private Service service;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CompletingOrderFrame5 frame = new CompletingOrderFrame5();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */

	public CompletingOrderFrame5() {
		init();

	}

	public CompletingOrderFrame5(Service service) {
		this.service = service;
		this.frame = this;
		init();
		progressBar.setValue(service.getCountCompletedOrderDetails());
		label.setText(String.valueOf(service.getCurrentPositionOrderDetails().get().getQuantity()));
		mnNewMenu.setText(service.getTmpPacker().getId());
		mnCart.setText(String.valueOf(service.getTmpContainer().getId()));
		setTitle("Completing#5");
	}

	public void init() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 265, 382);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(230, 230, 250));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		menuBar = new JMenuBar();
		menuBar.setBackground(Color.WHITE);
		menuBar.setBounds(0, 0, 248, 21);
		JMenu mnNewMenu_1 = new JMenu("Menu");
		menuBar.add(mnNewMenu_1);

		JMenuItem mntmOrderDetails = new JMenuItem("Order");
		mntmOrderDetails.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {
					GetOrderDetailsFrame godf = new GetOrderDetailsFrame(service, frame);
					frame.setVisible(false);
					godf.setVisible(true);

				} catch (Exception e) {

					e.printStackTrace();
				}

			}
		});
		mnNewMenu_1.add(mntmOrderDetails);

		JMenuItem mntmMenu = new JMenuItem("Logout");
		mntmMenu.setEnabled(false);
		mntmMenu.setHorizontalAlignment(SwingConstants.LEFT);
		mntmMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {
					service.logout();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				dispose();
			}
		});
		mnNewMenu_1.add(mntmMenu);

		menuBar.add(Box.createHorizontalGlue());
		contentPane.add(menuBar);
		menuBar.add(Box.createHorizontalGlue());
		contentPane.add(menuBar);

		mnCart = new JMenu("");
		mnCart.setEnabled(false);
		mnCart.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\cart.png"));
		menuBar.add(mnCart);

		mnNewMenu = new JMenu("");
		mnNewMenu.setEnabled(false);
		mnNewMenu.setBackground(Color.GRAY);
		mnNewMenu.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\user.png"));
		menuBar.add(mnNewMenu);

		lblNewLabel_1 = new JLabel("", SwingConstants.LEFT);
		lblNewLabel_1.setBackground(Color.WHITE);
		lblNewLabel_1.setFont(new Font("Sitka Text", Font.PLAIN, 12));
		lblNewLabel_1.setForeground(Color.RED);
		lblNewLabel_1.setBounds(62, 21, 152, 26);
		contentPane.add(lblNewLabel_1);

		JButton button = new JButton("ENTER");
		button.setFocusable(false);
		button.setForeground(new Color(255, 255, 255));
		button.setFont(new Font("Tahoma", Font.BOLD, 14));
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel_1.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\circlegreen.png"));
				lblNewLabel_1.setText("");

				if (textField.getText().equals("")) {

					lblNewLabel_1.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\circlered.png"));
					lblNewLabel_1.setText("Value is required.");

				} else {

					try {

						int tmp = service.finishCompletingCurrentOrderDetailsElement(textField.getText());

						if (tmp == 1) {

							CompletingOrderFrame1 cof1 = new CompletingOrderFrame1(service);
							cof1.setVisible(true);
							dispose();

						} else if (tmp == 2) {

							lblNewLabel_1.setIcon(
									new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\circlered.png"));
							lblNewLabel_1.setText("value is too high");

						}

						else if (tmp == 3) {

							lblNewLabel_1.setIcon(
									new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\circlegreen.png"));
							lblNewLabel_1.setText("");

							if (alert() == true) {

								int whatNext = service.skipBookInOrder();
								if (whatNext == 100) {
									CompletingOrderFrame6 cof6 = new CompletingOrderFrame6(service);
									dispose();
									cof6.setVisible(true);
								} else {
									CompletingOrderFrame1 cof1 = new CompletingOrderFrame1(service);
									cof1.setVisible(true);
									dispose();
								}

							}

						}

						else if (tmp == 100) {
							
							lblNewLabel_1.setIcon(
									new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\circlegreen.png"));
							lblNewLabel_1.setText("");

							CompletingOrderFrame6 cof6 = new CompletingOrderFrame6(service);
							dispose();
							cof6.setVisible(true);
						}

					} catch (Exception e1) {

						e1.printStackTrace();
					}
				}
			}

		});
		button.setBackground(new Color(0, 0, 128));
		button.setBounds(10, 235, 229, 63);
		contentPane.add(button);

		progressBar = new JProgressBar(0, service.getTmpOrderDetails().size());
		progressBar.setForeground(new Color(0, 0, 128));
		progressBar.setBounds(10, 323, 229, 5);
		contentPane.add(progressBar);

		JLabel lblRequiredCopies = new JLabel("Required copies:", SwingConstants.CENTER);
		lblRequiredCopies.setForeground(new Color(0, 0, 128));
		lblRequiredCopies.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblRequiredCopies.setBounds(10, 60, 182, 40);
		contentPane.add(lblRequiredCopies);

		label = new JLabel("", SwingConstants.CENTER);
		label.setBackground(new Color(255, 20, 147));
		label.setForeground(new Color(0, 0, 128));
		label.setFont(new Font("Tahoma", Font.BOLD, 20));
		label.setBounds(170, 60, 56, 40);
		contentPane.add(label);

		JLabel lblEnterNumberOf = new JLabel("Enter number of copies:", SwingConstants.LEFT);
		lblEnterNumberOf.setVerticalAlignment(SwingConstants.BOTTOM);
		lblEnterNumberOf.setForeground(new Color(0, 0, 128));
		lblEnterNumberOf.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblEnterNumberOf.setBounds(10, 122, 163, 40);
		contentPane.add(lblEnterNumberOf);

		textField = new JTextField();
		textField.setText("");
		textField.setHorizontalAlignment(SwingConstants.CENTER);
		textField.setForeground(new Color(0, 0, 128));
		textField.setFont(new Font("Tahoma", Font.BOLD, 26));
		textField.setColumns(5);
		textField.setBounds(10, 165, 229, 40);
		contentPane.add(textField);

		lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblNewLabel_1.setForeground(Color.RED);
		lblNewLabel_1.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\circlegreen.png"));
		lblNewLabel_1.setBounds(10, 21, 237, 14);
		contentPane.add(lblNewLabel_1);

	}

	public boolean alert() {

		Alert a = new Alert(this);

		return a.cancelItemOfOrder();
	}
}
