package pjwstk.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Optional;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import pjwstk.data.Alert;
import pjwstk.data.CompletationStatus;
import pjwstk.data.Service;
import pjwstk.entity.OrderDetails;

public class CompletingOrderFrame4 extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JMenuBar menuBar;
	private JMenu mnNewMenu;
	private JLabel lblNewLabel_1;

	private JTextField textField;
	private JLabel lblTitle_1;
	private JLabel label_1;
	private JLabel label;
	private JFrame frame;
	private JProgressBar progressBar;

	private JMenu mnCart;

	private Service service;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CompletingOrderFrame4 frame = new CompletingOrderFrame4();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */

	public CompletingOrderFrame4() {
		init();
		setTitle("Completing #4");
	}

	public CompletingOrderFrame4(Service service) {

		this.service = service;
		this.frame = this;
		init();
		setTitle("Completing #4");

		progressBar.setValue(service.getCountCompletedOrderDetails());

		mnNewMenu.setText(service.getTmpPacker().getId());
		lblTitle_1.setText(service.getCurrentPositionOrderDetails().get().getBook().getTitle());

		label_1.setText(service.getCurrentPositionOrderDetails().get().getBook().getAuthors().toString());
		label.setText(service.getCurrentPositionOrderDetails().get().getBook().getISBN());
		mnCart.setText(String.valueOf(service.getTmpContainer().getId()));
	}

	public void init() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 265, 382);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(230, 230, 250));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		menuBar = new JMenuBar();
		menuBar.setBackground(Color.WHITE);
		menuBar.setBounds(0, 0, 248, 21);

		JMenu mnNewMenu_1 = new JMenu("Menu");
		menuBar.add(mnNewMenu_1);

		JMenuItem mntmOrderDetails = new JMenuItem("Order");
		mntmOrderDetails.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {
					GetOrderDetailsFrame godf = new GetOrderDetailsFrame(service, frame);
					frame.setVisible(false);
					godf.setVisible(true);

				} catch (Exception e) {

					e.printStackTrace();
				}

			}
		});
		mnNewMenu_1.add(mntmOrderDetails);

		JMenuItem mntmMenu = new JMenuItem("Logout");
		mntmMenu.setEnabled(false);
		mntmMenu.setHorizontalAlignment(SwingConstants.LEFT);
		mntmMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {
					service.logout();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				dispose();
			}
		});
		mnNewMenu_1.add(mntmMenu);
		menuBar.add(Box.createHorizontalGlue());
		contentPane.add(menuBar);
		menuBar.add(Box.createHorizontalGlue());
		contentPane.add(menuBar);

		mnCart = new JMenu("");
		mnCart.setEnabled(false);
		mnCart.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\cart.png"));
		menuBar.add(mnCart);

		mnNewMenu = new JMenu("");
		mnNewMenu.setEnabled(false);
		mnNewMenu.setBackground(Color.GRAY);
		mnNewMenu.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\user.png"));
		menuBar.add(mnNewMenu);

		JLabel lblAuthor = new JLabel("Author:", SwingConstants.CENTER);
		lblAuthor.setForeground(new Color(0, 0, 128));
		lblAuthor.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblAuthor.setBounds(10, 95, 52, 21);
		contentPane.add(lblAuthor);

		JLabel lblIsbn = new JLabel("ISBN", SwingConstants.LEFT);
		lblIsbn.setForeground(new Color(0, 0, 128));
		lblIsbn.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblIsbn.setBounds(10, 115, 52, 21);
		contentPane.add(lblIsbn);

		label_1 = new JLabel("Author:", SwingConstants.LEFT);
		label_1.setForeground(new Color(0, 0, 128));
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 10));
		label_1.setBounds(72, 96, 167, 21);
		contentPane.add(label_1);

		JLabel lblTitle = new JLabel("Title:", SwingConstants.LEFT);
		lblTitle.setForeground(new Color(0, 0, 128));
		lblTitle.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblTitle.setBounds(10, 75, 52, 21);
		contentPane.add(lblTitle);

		lblTitle_1 = new JLabel("Title", SwingConstants.LEFT);
		lblTitle_1.setForeground(new Color(0, 0, 128));
		lblTitle_1.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblTitle_1.setBounds(72, 75, 167, 21);
		contentPane.add(lblTitle_1);

		label = new JLabel("ISBN", SwingConstants.LEFT);
		label.setForeground(new Color(0, 0, 128));
		label.setFont(new Font("Tahoma", Font.PLAIN, 13));
		label.setBounds(72, 115, 167, 21);
		contentPane.add(label);

		textField = new JTextField();
		textField.setText("");
		textField.setHorizontalAlignment(SwingConstants.CENTER);
		textField.setForeground(new Color(0, 0, 128));
		textField.setFont(new Font("Tahoma", Font.BOLD, 26));
		textField.setColumns(5);
		textField.setBounds(10, 169, 229, 40);
		contentPane.add(textField);

		JButton button = new JButton("ENTER");
		button.setFocusable(false);
		button.setForeground(new Color(255, 255, 255));
		button.setFont(new Font("Tahoma", Font.BOLD, 15));
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel_1.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\circlegreen.png"));
				lblNewLabel_1.setText("");

				if (service.checkEnteredISBN(textField.getText()) == (-1)) {
					if (alert() == true) {

						int whatNext = 0;

						try {
							whatNext = service.skipBookInOrder();
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

						if (whatNext == 60) {
							CompletingOrderFrame6 cof6 = new CompletingOrderFrame6(service);
							dispose();
							cof6.setVisible(true);
						} else {
							CompletingOrderFrame1 cof1 = new CompletingOrderFrame1(service);
							cof1.setVisible(true);
							dispose();
						}

					}
				}
				if (service.checkEnteredISBN(textField.getText()) == 1) {

					CompletingOrderFrame5 cof5 = new CompletingOrderFrame5(service);
					cof5.setVisible(true);
					dispose();
				}
				if (service.checkEnteredISBN(textField.getText()) == 0) {
					lblNewLabel_1.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\circlered.png"));
					lblNewLabel_1.setText("Wrong ISBN number");

				}
			}
		});
		button.setBackground(new Color(25, 25, 112));
		button.setBounds(10, 235, 229, 63);
		contentPane.add(button);

		progressBar = new JProgressBar(0, service.getTmpOrderDetails().size());
		progressBar.setForeground(new Color(0, 0, 128));
		progressBar.setBounds(10, 323, 229, 5);
		contentPane.add(progressBar);

		lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblNewLabel_1.setForeground(Color.RED);
		lblNewLabel_1.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\circlegreen.png"));
		lblNewLabel_1.setBounds(10, 21, 237, 14);
		contentPane.add(lblNewLabel_1);

	}

	public boolean alert() {

		Alert a = new Alert(this);

		return a.cancelItemOfOrder();
	}
}
