package pjwstk.gui;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import pjwstk.data.Service;

import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.SystemColor;
import javax.swing.JButton;
import javax.swing.JProgressBar;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CompletingOrderFrame3 extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JMenuBar menuBar;
	private JMenu mnNewMenu;
	private JLabel lblNewLabel_1;

	private JLabel lblNewLabel_3;
	private JTextField textField;
	private JButton button;
	private JProgressBar progressBar;
	private JMenu mnCart;
	private JFrame frame;

	private Service service;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CompletingOrderFrame3 frame = new CompletingOrderFrame3();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CompletingOrderFrame3() {
		init();
		setTitle("Completing #3");

	}

	public CompletingOrderFrame3(Service service) {
		this.service = service;
		this.frame = this;
		init();
		progressBar.setValue(service.getCountCompletedOrderDetails());
		setTitle("Completing #3");

		mnNewMenu.setText(service.getTmpPacker().getId());
		lblNewLabel_3.setText(
				service.getCurrentPositionOrderDetails().get().getBook().getCell().getAddressCell().toString());
		mnCart.setText(String.valueOf(service.getTmpContainer().getId()));

	}

	private void init() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 265, 382);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(230, 230, 250));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		menuBar = new JMenuBar();
		menuBar.setBackground(Color.WHITE);
		menuBar.setBounds(0, 0, 248, 21);

		JMenu mnNewMenu_1 = new JMenu("Menu");
		menuBar.add(mnNewMenu_1);

		JMenuItem mntmOrderDetails = new JMenuItem("Order");
		mntmOrderDetails.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {
					GetOrderDetailsFrame godf = new GetOrderDetailsFrame(service, frame);
					frame.setVisible(false);
					godf.setVisible(true);

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});
		mnNewMenu_1.add(mntmOrderDetails);

		JMenuItem mntmMenu = new JMenuItem("Logout");
		mntmMenu.setEnabled(false);
		mntmMenu.setHorizontalAlignment(SwingConstants.LEFT);
		mntmMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {
					service.logout();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				dispose();
			}
		});
		mnNewMenu_1.add(mntmMenu);
		menuBar.add(Box.createHorizontalGlue());
		contentPane.add(menuBar);
		menuBar.add(Box.createHorizontalGlue());
		contentPane.add(menuBar);

		mnCart = new JMenu("");
		mnCart.setEnabled(false);
		mnCart.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\cart.png"));
		menuBar.add(mnCart);

		mnNewMenu = new JMenu("");
		mnNewMenu.setEnabled(false);
		mnNewMenu.setBackground(Color.GRAY);
		mnNewMenu.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\user.png"));
		menuBar.add(mnNewMenu);

		lblNewLabel_1 = new JLabel("", JLabel.CENTER);
		lblNewLabel_1.setBackground(Color.WHITE);
		lblNewLabel_1.setFont(new Font("Sitka Text", Font.PLAIN, 12));
		lblNewLabel_1.setForeground(Color.RED);
		lblNewLabel_1.setBounds(62, 21, 152, 26);
		contentPane.add(lblNewLabel_1);



		JLabel lblCellCode = new JLabel("Cell code:", SwingConstants.CENTER);
		lblCellCode.setForeground(new Color(0, 0, 128));
		lblCellCode.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblCellCode.setBounds(0, 58, 248, 33);
		contentPane.add(lblCellCode);

		lblNewLabel_3 = new JLabel("", SwingConstants.CENTER);
		lblNewLabel_3.setForeground(new Color(25, 25, 112));
		lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 30));
		lblNewLabel_3.setBounds(10, 94, 229, 40);
		contentPane.add(lblNewLabel_3);

		textField = new JTextField();
		textField.setText("");
		textField.setHorizontalAlignment(SwingConstants.CENTER);
		textField.setForeground(new Color(0, 0, 128));
		textField.setFont(new Font("Tahoma", Font.BOLD, 26));
		textField.setColumns(5);
		textField.setBounds(10, 159, 229, 40);
		contentPane.add(textField);

		button = new JButton("ENTER");
		button.setFocusable(false);
		button.setForeground(new Color(255, 255, 255));
		button.setFont(new Font("Tahoma", Font.BOLD, 15));
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblNewLabel_1.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\circlegreen.png"));
				lblNewLabel_1.setText("");
				JOptionPane jp = new JOptionPane();
				jp.setVisible(true);
				if (textField.getText().equals(service.getCurrentPositionOrderDetails().get().getBook().getCell()
						.getAddressCell().toString())) {

					CompletingOrderFrame4 cof4 = new CompletingOrderFrame4(service);
					cof4.setVisible(true);
					dispose();

				} else {

					System.out.println(service.getCurrentPositionOrderDetails().get().getBook().getCell()
							.getAddressCell().toString());

				
					lblNewLabel_1.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\circlered.png"));
					lblNewLabel_1.setText("Invalid entered number.");
				}
			}
		});
		button.setBackground(new Color(0, 0, 128));
		button.setBounds(10, 239, 229, 63);
		contentPane.add(button);

		progressBar = new JProgressBar(0, service.getTmpOrderDetails().size());
		progressBar.setForeground(new Color(0, 0, 128));
		progressBar.setBounds(10, 327, 229, 5);
		contentPane.add(progressBar);
		
		lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblNewLabel_1.setForeground(Color.RED);
		lblNewLabel_1.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\circlegreen.png"));
		lblNewLabel_1.setBounds(10, 21, 237, 14);
		contentPane.add(lblNewLabel_1);


	}
}
