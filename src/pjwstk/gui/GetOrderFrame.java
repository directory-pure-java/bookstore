package pjwstk.gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import pjwstk.data.ObjectPlus;
import pjwstk.data.Service;
import pjwstk.entity.Order;
import pjwstk.entity.OrderDetails;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Optional;
import java.awt.event.ActionEvent;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import java.awt.Color;
import javax.swing.SwingConstants;
import javax.swing.JMenuItem;

public class GetOrderFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JMenuBar menuBar;
	private JMenu mnNewMenu;
	private JLabel lblNewLabel_1;

	private Service service;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GetOrderFrame frame = new GetOrderFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * 
	 * @param tmpPacker
	 */
	public GetOrderFrame() {
		init();
	}

	public GetOrderFrame(Service service) {
		this.service = service;

		init();

		mnNewMenu.setText(service.getTmpPacker().getId());

	}

	void init() {

		setTitle("Packer");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 263, 382);

		contentPane = new JPanel();
		contentPane.setBackground(new Color(230, 230, 250));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		getContentPane().requestFocusInWindow();

		JLabel lblSelectActivity = new JLabel("Select activity");
		lblSelectActivity.setForeground(new Color(0, 0, 128));
		lblSelectActivity.setHorizontalAlignment(SwingConstants.CENTER);
		lblSelectActivity.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblSelectActivity.setBounds(10, 110, 229, 26);
		contentPane.add(lblSelectActivity);

		JButton btnGetOrder = new JButton("GET ORDER");
		btnGetOrder.setForeground(SystemColor.window);
		btnGetOrder.setFocusable(false);
		btnGetOrder.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnGetOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel_1.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\circlegreen.png"));
				lblNewLabel_1.setText("");

				try {

					Optional<Order> o = service.getOrder();

					if (o.equals(Optional.empty())) {
						lblNewLabel_1
								.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\circlered.png"));
						lblNewLabel_1.setText("No orders in the system.");

					} else {
						List<OrderDetails> tmpOrderDetails = OrderDetails.getOrdersDetails(o.get());
						service.setTmpOrderDetails(tmpOrderDetails);
						GetContainerFrame gcf = new GetContainerFrame(service);
						gcf.setVisible(true);
						dispose();
					}
				} catch (Exception e1) {

					e1.printStackTrace();
				}
			}
		});
		btnGetOrder.setBackground(new Color(25, 25, 112));
		btnGetOrder.setBounds(10, 271, 229, 63);
		contentPane.add(btnGetOrder);

		menuBar = new JMenuBar();
		menuBar.setBackground(Color.WHITE);
		menuBar.setBounds(-1, 0, 248, 21);

		JMenu mnNewMenu_1 = new JMenu("Menu");
		mnNewMenu_1.setFont(new Font("Dialog", Font.PLAIN, 12));
		menuBar.add(mnNewMenu_1);
		menuBar.add(Box.createHorizontalGlue());
		contentPane.add(menuBar);

		JMenuItem mntmOrderDetails = new JMenuItem("Order");
		mntmOrderDetails.setBackground(SystemColor.control);
		mntmOrderDetails.setSelected(true);
		mntmOrderDetails.setEnabled(false);
		mntmOrderDetails.setFont(new Font("Dialog", Font.PLAIN, 12));
		mnNewMenu_1.add(mntmOrderDetails);

		JMenuItem mntmMenu = new JMenuItem("Logout");
		mntmMenu.setFont(new Font("Dialog", Font.PLAIN, 12));
		mntmMenu.setHorizontalAlignment(SwingConstants.LEFT);
		mntmMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {
					service.logout();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				dispose();

			}
		});
		mnNewMenu_1.add(mntmMenu);
		mnNewMenu_1.add(mntmMenu);
		menuBar.add(Box.createHorizontalGlue());
		contentPane.add(menuBar);

		mnNewMenu = new JMenu("");
		mnNewMenu.setEnabled(false);
		mnNewMenu.setBackground(Color.GRAY);
		mnNewMenu.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\user.png"));
		menuBar.add(mnNewMenu);

		lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblNewLabel_1.setForeground(Color.RED);
		lblNewLabel_1.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\circlegreen.png"));
		lblNewLabel_1.setBounds(10, 21, 237, 14);
		contentPane.add(lblNewLabel_1);

	}
}
