package pjwstk.gui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;


import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import pjwstk.data.Service;

import java.awt.SystemColor;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JProgressBar;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CompletingOrderFrame2 extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JMenuBar menuBar;
	private JMenu mnNewMenu;
	private JLabel lblNewLabel_1;
	private JLabel lblNewLabel_3;
	private JLabel label;
	private JTextField textField;
	private JMenu mnCart;
	private JFrame frame;
	private JProgressBar progressBar;
	

	private Service service;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CompletingOrderFrame2 frame = new CompletingOrderFrame2();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CompletingOrderFrame2() {
		init();
		setTitle("Completing #2");
	}

	public CompletingOrderFrame2(Service service) {
		this.service = service;
		this.frame= this;
		init();
		progressBar.setValue(service.getCountCompletedOrderDetails());
		
		setTitle("Completing #2");
		
		mnNewMenu.setText(service.getTmpPacker().getId());
		label.setText(String.valueOf(service.getCurrentPositionOrderDetails().get().getBook().getCell().getAddressBookshelf()
				.getColumn()));
		mnCart.setText(String.valueOf(service.getTmpContainer().getId()));
	}

	public void init() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 265, 382);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(230, 230, 250));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		

		menuBar = new JMenuBar();
		menuBar.setBackground(Color.WHITE);
		menuBar.setBounds(0, 0, 248, 21);
		
		JMenu mnNewMenu_1 = new JMenu("Menu");
		menuBar.add(mnNewMenu_1);
		
				JMenuItem mntmOrderDetails = new JMenuItem("Order");
				mntmOrderDetails.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {

						try {
							GetOrderDetailsFrame godf = new GetOrderDetailsFrame(service, frame);
							frame.setVisible(false);
							godf.setVisible(true);

						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
				});
				mnNewMenu_1.add(mntmOrderDetails);
		menuBar.add(Box.createHorizontalGlue());
		contentPane.add(menuBar);

		JMenuItem mntmMenu = new JMenuItem("Logout");
		mntmMenu.setEnabled(false);
		mntmMenu.setHorizontalAlignment(SwingConstants.LEFT);
		mntmMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {
					service.logout();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				dispose();
			}
		});
		mnNewMenu_1.add(mntmMenu);
		
		mnCart = new JMenu("");
		mnCart.setEnabled(false);
		mnCart.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\cart.png"));
		menuBar.add(mnCart);
	
		
		mnNewMenu = new JMenu("");
		mnNewMenu.setEnabled(false);
		mnNewMenu.setBackground(Color.GRAY);
		mnNewMenu.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\user.png"));
		menuBar.add(mnNewMenu);

		JLabel lblBookshelfColumnCode = new JLabel("Bookshelf column code:", JLabel.CENTER);
		lblBookshelfColumnCode.setForeground(new Color(0, 0, 128));
		lblBookshelfColumnCode.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblBookshelfColumnCode.setBounds(10, 75, 229, 40);
		contentPane.add(lblBookshelfColumnCode);

		label = new JLabel("", JLabel.CENTER);
		label.setForeground(new Color(0, 0, 128));
		label.setFont(new Font("Tahoma", Font.BOLD, 30));
		label.setBounds(10, 114, 229, 40);
		contentPane.add(label);

		textField = new JTextField();
		textField.setHorizontalAlignment(JTextField.CENTER);
		textField.setText("");
		textField.setForeground(new Color(0, 0, 128));
		textField.setFont(new Font("Tahoma", Font.BOLD, 26));
		textField.setColumns(5);
		textField.setBounds(10, 187, 229, 38);
		contentPane.add(textField);

		JButton button = new JButton("ENTER");
		button.setFocusable(false);
		button.setForeground(new Color(255, 255, 255));
		button.setFont(new Font("Tahoma", Font.BOLD, 14));
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				lblNewLabel_1.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\circlegreen.png"));
				lblNewLabel_1.setText("");

						
				if (textField.getText().equals(String.valueOf(service.getCurrentPositionOrderDetails().get().getBook().getCell().getAddressBookshelf()
						.getColumn()))) {

					
					
					CompletingOrderFrame3 cof3 = new CompletingOrderFrame3(new Service(service.getTmpPacker(),
							service.getTmpOrderDetails(), service.getTmpContainer(),null));
					cof3.setVisible(true);
					dispose();

				} else {
				
					lblNewLabel_1.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\circlered.png"));
					lblNewLabel_1.setText("Invalid column number.");
				}


			}
		});
		button.setBackground(new Color(0, 0, 128));
		button.setBounds(10, 248, 229, 63);
		contentPane.add(button);

		progressBar = new JProgressBar(0, service.getTmpOrderDetails().size());
		progressBar.setForeground(new Color(0, 0, 128));
		progressBar.setBounds(10, 327, 229, 5);
		contentPane.add(progressBar);
		
		lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblNewLabel_1.setForeground(Color.RED);
		lblNewLabel_1.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\circlegreen.png"));
		lblNewLabel_1.setBounds(10, 21, 237, 14);
		contentPane.add(lblNewLabel_1);
		
		
	}
}
