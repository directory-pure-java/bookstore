package pjwstk.gui;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import pjwstk.data.AddressCell;
import pjwstk.data.Service;

import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JProgressBar;

public class CompletingOrderFrame6 extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JMenuBar menuBar;
	private JMenu mnNewMenu;
	private JMenu mnCart;
	private JFrame frame;
	private JLabel lblNewLabel_1;
	private JLabel lblNewLabel_2;

	private Service service;
	private JTextField textField;
	private JProgressBar progressBar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CompletingOrderFrame6 frame = new CompletingOrderFrame6();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CompletingOrderFrame6() {
		init();
	}

	public CompletingOrderFrame6(Service service) {
		this.service = service;
		this.frame = this;
		init();
		progressBar.setValue(service.getCountCompletedOrderDetails());
		setTitle("Completing#6");
	}

	private void init() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 265, 382);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(230, 230, 250));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		menuBar = new JMenuBar();
		menuBar.setBackground(Color.WHITE);
		menuBar.setBounds(0, 0, 248, 21);
		JMenu mnNewMenu_1 = new JMenu("Menu");
		menuBar.add(mnNewMenu_1);

		JMenuItem mntmOrderDetails = new JMenuItem("Order");
		mntmOrderDetails.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {
					GetOrderDetailsFrame godf = new GetOrderDetailsFrame(service, frame);
					frame.setVisible(false);
					godf.setVisible(true);

				} catch (Exception e) {

					e.printStackTrace();
				}

			}
		});
		mnNewMenu_1.add(mntmOrderDetails);

		JMenuItem mntmMenu = new JMenuItem("Logout");
		mntmMenu.setEnabled(false);
		mntmMenu.setHorizontalAlignment(SwingConstants.LEFT);
		mntmMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {
					service.logout();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				dispose();
			}
		});
		mnNewMenu_1.add(mntmMenu);

		menuBar.add(Box.createHorizontalGlue());
		contentPane.add(menuBar);
		menuBar.add(Box.createHorizontalGlue());
		contentPane.add(menuBar);

		mnCart = new JMenu("");
		mnCart.setEnabled(false);
		mnCart.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\cart.png"));
		menuBar.add(mnCart);

		mnNewMenu = new JMenu("");
		mnNewMenu.setEnabled(false);
		mnNewMenu.setBackground(Color.GRAY);
		mnNewMenu.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\user.png"));
		menuBar.add(mnNewMenu);

		lblNewLabel_1 = new JLabel("", JLabel.CENTER);
		lblNewLabel_1.setBackground(Color.WHITE);
		lblNewLabel_1.setFont(new Font("Sitka Text", Font.PLAIN, 12));
		lblNewLabel_1.setForeground(Color.RED);
		lblNewLabel_1.setBounds(62, 21, 152, 26);
		contentPane.add(lblNewLabel_1);

		lblNewLabel_2 = new JLabel("");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel_2.setBounds(10, 21, 56, 26);
		contentPane.add(lblNewLabel_2);

		JLabel lblSetAsideThe = new JLabel("Set aside the container", SwingConstants.CENTER);
		lblSetAsideThe.setForeground(new Color(0, 0, 128));
		lblSetAsideThe.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblSetAsideThe.setBounds(10, 79, 229, 32);
		contentPane.add(lblSetAsideThe);

		textField = new JTextField();
		textField.setText("");
		textField.setHorizontalAlignment(SwingConstants.CENTER);
		textField.setForeground(new Color(0, 0, 128));
		textField.setFont(new Font("Tahoma", Font.BOLD, 26));
		textField.setColumns(5);
		textField.setBounds(10, 174, 229, 40);
		contentPane.add(textField);

		JLabel lblEnterBookshelfCell = new JLabel("Enter bookshelf cell code:", SwingConstants.LEFT);
		lblEnterBookshelfCell.setVerticalAlignment(SwingConstants.BOTTOM);
		lblEnterBookshelfCell.setForeground(new Color(0, 0, 128));
		lblEnterBookshelfCell.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblEnterBookshelfCell.setBounds(10, 131, 163, 40);
		contentPane.add(lblEnterBookshelfCell);

		JButton button = new JButton("ENTER");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel_1.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\circlegreen.png"));
				lblNewLabel_1.setText("");

				String tmp = textField.getText();
				int col = Integer.parseInt(tmp.substring(0, tmp.indexOf("-")));
				int row = Integer.parseInt(tmp.substring(tmp.indexOf("-") + 1, tmp.length()));

				try {
					boolean flag = service.putAsaidContainer(new AddressCell(col, row));

					if (flag) {
						service.clean();
						GetOrderFrame gof = new GetOrderFrame(service);
						gof.setVisible(true);
						dispose();

					} else {
						lblNewLabel_1
								.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\circlered.png"));
						lblNewLabel_1.setText("Invalid Cell address.");

					}

				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		button.setFocusable(false);
		button.setForeground(Color.WHITE);
		button.setFont(new Font("Tahoma", Font.BOLD, 14));
		button.setBackground(new Color(25, 25, 112));
		button.setBounds(10, 239, 229, 63);
		contentPane.add(button);

		progressBar = new JProgressBar(0, service.getTmpOrderDetails().size());
		progressBar.setForeground(new Color(0, 0, 128));
		progressBar.setBounds(10, 327, 229, 5);
		contentPane.add(progressBar);

		lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblNewLabel_1.setForeground(Color.RED);
		lblNewLabel_1.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\circlegreen.png"));
		lblNewLabel_1.setBounds(10, 21, 237, 14);
		contentPane.add(lblNewLabel_1);

	}
}
