package pjwstk.gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.HeadlessException;

import javax.swing.JButton;

import java.util.List;

import javax.swing.JTable;

import javax.swing.JScrollPane;

import javax.swing.table.DefaultTableModel;

import pjwstk.data.Service;
import pjwstk.data.TableCellColorRender;

import pjwstk.entity.OrderDetails;


import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;


public class GetOrderDetailsFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unused")
	private List<OrderDetails> tmpOrderDetails;

	private JPanel contentPane;
	private JTable table;

	@SuppressWarnings("unused")
	private Service service;
	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GetOrderDetailsFrame frame = new GetOrderDetailsFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public GetOrderDetailsFrame() throws HeadlessException {
		super();

		init();

	}

	/**
	 * Create the frame.
	 * 
	 * @throws Exception
	 */
	public GetOrderDetailsFrame(Service service, JFrame frame) throws Exception {

		init();

		this.service = service;
		this.frame = frame;

		for (int i = 0; i < service.getTmpOrderDetails().size(); i++) {
			DefaultTableModel model = (DefaultTableModel) table.getModel();
			model.addRow(new Object[] { i, service.getTmpOrderDetails().get(i).getBook().getISBN(),
					service.getTmpOrderDetails().get(i).getQuantity(),
					service.getTmpOrderDetails().get(i).getBook().getCell(),
					service.getTmpOrderDetails().get(i).getStatus() });

		}

	}

	void init() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 265, 382);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(230, 230, 250));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblOrderDetails = new JLabel("Order details");
		lblOrderDetails.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblOrderDetails.setBounds(68, 11, 123, 26);
		contentPane.add(lblOrderDetails);

		JButton btnBack = new JButton("Back");
		btnBack.setFocusable(false);
		btnBack.setForeground(new Color(255, 255, 255));
		btnBack.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				frame.setVisible(true);
				dispose();
			}
		});
		btnBack.setBackground(new Color(0, 0, 128));
		btnBack.setBounds(10, 262, 239, 70);
		contentPane.add(btnBack);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 38, 239, 213);
		contentPane.add(scrollPane);

		table = new JTable();
		table.setFont(new Font("Tahoma", Font.PLAIN, 10));
		scrollPane.setViewportView(table);

		TableCellColorRender tccr = new TableCellColorRender();
		table.setDefaultRenderer(Object.class, tccr);

		table.setModel(new DefaultTableModel(new Object[][] {}, new String[] { "#", "ISBN", "Q", "L", "S" }) {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			Class[] columnTypes = new Class[] { String.class, String.class, String.class, String.class, String.class };

			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});

		table.getColumnModel().getColumn(0).setPreferredWidth(25);
		table.getColumnModel().getColumn(1).setPreferredWidth(75);
		table.getColumnModel().getColumn(2).setPreferredWidth(30);
		table.getColumnModel().getColumn(3).setPreferredWidth(75);
		table.getColumnModel().getColumn(4).setPreferredWidth(25);

		// tccr.setHorizontalAlignment(JLabel.CENTER);

	}
}
