package pjwstk.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import pjwstk.data.ContainerStatus;
import pjwstk.data.EmployeeStatus;
import pjwstk.data.ObjectPlus;
import pjwstk.data.OrderStatus;
import pjwstk.data.Service;
import pjwstk.entity.Container;
import pjwstk.entity.Order;
import pjwstk.entity.OrderDetails;
import pjwstk.entity.actors.Packer;

import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import java.awt.Font;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.awt.event.ActionEvent;
import javax.swing.JProgressBar;
import javax.swing.Timer;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import java.awt.Component;
import javax.swing.SwingConstants;

public class GetContainerFrame extends JFrame {

	static GetContainerFrame frame;

	Packer tmpPacker;
	List<OrderDetails> tmpOrderDetails;

	private JPanel contentPane;
	private JMenuBar menuBar;
	private JMenu mnNewMenu;
	private JTextField textField;
	private JLabel lblNewLabel_1;

	private Service service;

	/**
	 * Launch the application.
	 */

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new GetContainerFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * 
	 * @param tmpOrderDetails
	 * @param tmpPacker2
	 */

	public GetContainerFrame() {

		init();
		setTitle("Find container");
	}

	public GetContainerFrame(Service service) {

		this.service = service;
		frame = this;

		init();

		setTitle("Find container");
		mnNewMenu.setText(service.getTmpPacker().getId());
	}

	void init() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 265, 382);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(230, 230, 250));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblSelectActions = new JLabel("Get container");
		lblSelectActions.setForeground(new Color(0, 0, 128));
		lblSelectActions.setHorizontalAlignment(SwingConstants.CENTER);
		lblSelectActions.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblSelectActions.setBounds(10, 68, 239, 26);
		contentPane.add(lblSelectActions);

		JButton btnSelectContainer = new JButton("ENTER");
		btnSelectContainer.setFocusable(false);
		btnSelectContainer.setForeground(SystemColor.textHighlightText);
		btnSelectContainer.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnSelectContainer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				lblNewLabel_1.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\circlegreen.png"));
				lblNewLabel_1.setText("");

				try {

					if ((textField.getText().equals(""))) {
						System.out.println("Container id is required");
						lblNewLabel_1
								.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\circlered.png"));
						lblNewLabel_1.setText("Container id is required.");
					} else
						try {
							service.getContainer(new Integer(textField.getText()));
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}


				if (service.getTmpContainer() == null) {
					lblNewLabel_1.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\circlered.png"));
					lblNewLabel_1.setText("Take another container.");
				} else {
					
					
					CompletingOrderFrame1 cof1 = new CompletingOrderFrame1(service);
					cof1.setVisible(true);
					dispose();
				}


			}
		});
		btnSelectContainer.setBackground(new Color(25, 25, 112));
		btnSelectContainer.setBounds(10, 279, 239, 63);
		contentPane.add(btnSelectContainer);

		menuBar = new JMenuBar();
		menuBar.setBackground(Color.WHITE);
		menuBar.setBounds(0, 0, 259, 21);

		JMenu mnNewMenu_1 = new JMenu("Menu");
		menuBar.add(mnNewMenu_1);
		menuBar.add(Box.createHorizontalGlue());
		contentPane.add(menuBar);

		JMenuItem mntmOrderDetails = new JMenuItem("Order");
		mntmOrderDetails.setFont(new Font("Segoe UI", Font.PLAIN, 11));
		mntmOrderDetails.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {
					GetOrderDetailsFrame godf = new GetOrderDetailsFrame(service, frame);
					frame.setVisible(false);
					godf.setVisible(true);

				} catch (Exception e) {

					e.printStackTrace();
				}

			}
		});

		mnNewMenu_1.add(mntmOrderDetails);

		JMenuItem mntmMenu = new JMenuItem("Logout");
		mntmMenu.setSelected(true);
		mntmMenu.setFont(new Font("Segoe UI Emoji", Font.PLAIN, 11));
		mntmMenu.setHorizontalAlignment(SwingConstants.LEFT);
		mntmMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {
					service.logout();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				dispose();

			}
		});
		mnNewMenu_1.add(mntmMenu);
		mnNewMenu_1.add(mntmMenu);
		menuBar.add(Box.createHorizontalGlue());
		contentPane.add(menuBar);

		Component horizontalGlue = Box.createHorizontalGlue();
		horizontalGlue.setEnabled(false);
		menuBar.add(horizontalGlue);
		contentPane.add(menuBar);
		menuBar.add(Box.createHorizontalGlue());
		contentPane.add(menuBar);
		menuBar.add(Box.createHorizontalGlue());
		contentPane.add(menuBar);

		mnNewMenu = new JMenu("");
		mnNewMenu.setEnabled(false);
		mnNewMenu.setBackground(Color.GRAY);
		mnNewMenu.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\user.png"));
		menuBar.add(mnNewMenu);

		JLabel lblNewLabel = new JLabel("Enter container id");
		lblNewLabel.setForeground(new Color(0, 0, 128));
		lblNewLabel.setHorizontalAlignment(SwingConstants.LEFT);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel.setBounds(10, 149, 239, 21);
		contentPane.add(lblNewLabel);

		textField = new JTextField();
		textField.setForeground(new Color(0, 0, 128));
		textField.setHorizontalAlignment(JTextField.CENTER);
		textField.setFont(new Font("Tahoma", Font.BOLD, 26));
		textField.setBounds(10, 171, 239, 39);
		contentPane.add(textField);
		textField.setColumns(10);

		lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblNewLabel_1.setForeground(Color.RED);
		lblNewLabel_1.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\circlegreen.png"));
		lblNewLabel_1.setBounds(10, 21, 237, 14);
		contentPane.add(lblNewLabel_1);

	}
}
