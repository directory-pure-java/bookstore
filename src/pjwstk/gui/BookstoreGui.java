package pjwstk.gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Optional;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import java.awt.SystemColor;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import pjwstk.data.EmployeeStatus;
import pjwstk.data.ObjectPlus;
import pjwstk.data.Service;
import pjwstk.entity.actors.Employee;
import pjwstk.entity.actors.Packer;

import javax.swing.JMenuItem;
import javax.swing.ImageIcon;

public class BookstoreGui {

	private JFrame frmBookstore;
	private JTextField idField;
	private JPasswordField passwordField;
	private JLabel lblNewLabel_1;

	private Service service;
	private static int scannerId;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BookstoreGui window = new BookstoreGui(args[0]);
					scannerId = Integer.parseInt(args[0]);
					window.frmBookstore.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	/**
	 * Create the application.
	 * 
	 * @throws Exception
	 * @wbp.parser.entryPoint
	 */

	public BookstoreGui(String s) throws Exception {

		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * 
	 * @throws Exception
	 */

	private void initialize() throws Exception {

		service = new Service(null, null, null, null);

		frmBookstore = new JFrame();

		frmBookstore.getContentPane().setBackground(new Color(230, 230, 250));
		frmBookstore.setTitle("Bookstore");
		frmBookstore.setResizable(false);
		frmBookstore.setBounds(100, 100, 265, 382);
		frmBookstore.setResizable(false);
		frmBookstore.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmBookstore.getContentPane().setLayout(null);

		JLabel lblNewLabel = new JLabel("User id");
		lblNewLabel.setHorizontalAlignment(SwingConstants.LEFT);
		lblNewLabel.setForeground(new Color(0, 0, 128));
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNewLabel.setBounds(10, 83, 225, 26);
		frmBookstore.getContentPane().add(lblNewLabel);

		JButton btnNewButton = new JButton("ENTER");
		btnNewButton.setFocusable(false);
		btnNewButton.setForeground(new Color(255, 255, 255));
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnNewButton.setBackground(new Color(25, 25, 112));

		btnNewButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				lblNewLabel_1.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\circlegreen.png"));
				lblNewLabel_1.setText("");
				boolean canLogin = false;

				Optional<Packer> tmpPacker = null;
				try {

					tmpPacker = Employee.login(idField.getText(), passwordField.getText(), scannerId);

				} catch (Exception e) {

					e.printStackTrace();
				}

				if (tmpPacker.equals(Optional.empty())) {

				} else
					canLogin = true;

				if (!canLogin) {
					lblNewLabel_1.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\circlered.png"));
					lblNewLabel_1.setText("invalid id or password!");

				} else {

					frmBookstore.setVisible(false);
					GetOrderFrame gof = new GetOrderFrame(new Service(tmpPacker.get(), null, null, frmBookstore));
					gof.setVisible(true);

				}
			}
		});
		btnNewButton.setBounds(10, 279, 239, 63);
		frmBookstore.getContentPane().add(btnNewButton);

		idField = new JTextField();
		idField.setForeground(new Color(0, 0, 128));
		idField.setFont(new Font("Tahoma", Font.BOLD, 14));
		idField.setHorizontalAlignment(JTextField.CENTER);
		idField.setBounds(10, 108, 239, 39);
		frmBookstore.getContentPane().add(idField);
		idField.setColumns(10);

		passwordField = new JPasswordField();
		passwordField.setForeground(new Color(0, 0, 128));
		passwordField.setFont(new Font("Tahoma", Font.BOLD, 14));
		passwordField.setHorizontalAlignment(JTextField.CENTER);
		passwordField.setBounds(10, 195, 239, 39);
		frmBookstore.getContentPane().add(passwordField);

		JLabel lblPassword = new JLabel("Password");
		lblPassword.setVerticalAlignment(SwingConstants.BOTTOM);
		lblPassword.setForeground(new Color(0, 0, 128));
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblPassword.setBounds(10, 168, 225, 26);
		frmBookstore.getContentPane().add(lblPassword);

		lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblNewLabel_1.setForeground(new Color(255, 0, 0));
		lblNewLabel_1.setIcon(new ImageIcon("D:\\thesisWorkspace\\bookstore\\src\\static\\circlegreen.png"));
		lblNewLabel_1.setBounds(10, 0, 237, 26);
		frmBookstore.getContentPane().add(lblNewLabel_1);

		frmBookstore.getContentPane().requestFocusInWindow();
	}
}
