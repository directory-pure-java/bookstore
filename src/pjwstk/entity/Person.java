package pjwstk.entity;

import java.io.FileNotFoundException;
import java.io.IOException;

import pjwstk.data.ObjectPlus;

public abstract class Person extends ObjectPlus {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private String surname;

	public abstract String showInfo();
	
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) throws Exception {
		if (name == null)
			throw new Exception("Err: name is required");
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) throws Exception {
		if (surname == null)
			throw new Exception("Err: surname is required.");

		this.surname = surname;
	}

	
	public Person(String name, String surname) throws FileNotFoundException, IOException {
		super();
		this.name = name;
		this.surname = surname;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", surname=" + surname + "]";
	}


}
