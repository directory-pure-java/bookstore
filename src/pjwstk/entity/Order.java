package pjwstk.entity;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.print.attribute.standard.DateTimeAtCompleted;

import pjwstk.data.CompletationStatus;
import pjwstk.data.ContainerStatus;
import pjwstk.data.ObjectPlus;
import pjwstk.data.OrderStatus;
import pjwstk.entity.actors.Customer;
import pjwstk.entity.actors.Packer;

public class Order extends ObjectPlus implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Date dateOfOrder;
	private OrderStatus status;
	private Customer customer;
	private Packer packer;
	private Container container;

	private List<OrderDetails> orderDetails = new ArrayList<>();

	public Order() throws Exception {
		super();

		this.dateOfOrder = new Date();
		setStatus(OrderStatus.NONECUSTOMER);
	}

	public static Order createOrder() throws Exception {

		return new Order();
	}

	public static Order find(String email, OrderStatus status) throws ClassNotFoundException, IOException {

		Order tmpOrder;
		

		Iterable<Order> source = ObjectPlus.getExtent(Order.class);
		List<Order> target = new ArrayList<>();
		source.forEach(target::add);

		tmpOrder = target.stream().filter(v ->  v.customer.getEmail().equals(email))
				.findFirst().get();

		return tmpOrder;

	}


	public void addOrderDetails(OrderDetails newOrderDetail) throws Exception {
		// Check if we already have the info
		if (!orderDetails.contains(newOrderDetail)) {
			orderDetails.add(newOrderDetail);
			// Add the reverse connection
			newOrderDetail.setOrder(this);
		}
	}

	public void setPacker(Packer newPacker) throws Exception {

		if (newPacker == null) {
			throw new Exception("Err: packer in order is required.");
		}

		if (this.packer == null) {
			this.packer = newPacker;
			this.packer.addOrder(this);
		}

		else {
			this.packer.getOrders().remove(this);
			this.packer = newPacker;
			this.packer.getOrders().add(this);
		}
	}
	public void setCustomer(Customer newCustomer) throws Exception {

		setStatus(OrderStatus.NONEBOOK);

		if (this.customer == null) {
			this.customer = newCustomer;
			this.customer.addOrder(this);
		}

		else {
			this.customer.getOrders().remove(this);
			this.customer = newCustomer;
			this.customer.getOrders().add(this);
		}
		ObjectPlus.writeExtents();

	}

	public void setContainer(Container newContainer) {
		if (this.container == null) {
			this.container = newContainer;
			this.container.setOrder(this);
		} else if (!this.container.equals(newContainer)) {
			this.container.setOrder(null);
			this.container = newContainer;
			this.container.setOrder(this);
		}
	}

	public static Optional<Order> getFirstOrder() throws Exception {



		Iterable<Order> source = ObjectPlus.getExtent(Order.class);
		List<Order> target = new ArrayList<>();
		source.forEach(target::add);

		Comparator<Order> comparator = Comparator.comparing(Order::getDateOfOrder);

		Optional<Order> tmp = target.stream().filter(v -> v.getStatus().equals(OrderStatus.PENDING)).min(comparator);

		if (!tmp.equals(Optional.empty())) {
			tmp.get().setStatus(OrderStatus.INPOGRESS);
						
		return tmp;
		} else

			return tmp.empty();
	}

	public int getCountCompletedOrderDetails() {

		return (int) this.getOrderDetails().stream().filter(
				v -> v.getStatus().equals(CompletationStatus.OK) || v.getStatus().equals(CompletationStatus.NON))
				.count();

	}

	public OrderStatus getStatus() {
		return status;
	}

	public void setStatus(OrderStatus status) throws Exception {
		if (status == null)
			throw new Exception("Err: Order status  is required");
		this.status = status;
	}

	public Customer getCustomer() {
		return customer;
	}

	public Date getDateOfOrder() {
		return dateOfOrder;
	}

	public List<OrderDetails> getOrderDetails() {
		return orderDetails;
	}

	public void setOrderDetails(List<OrderDetails> orderDetails) {
		this.orderDetails = orderDetails;
	}

	public Packer getPacker() {
		return packer;
	}

	public Container getContainer() {
		return container;
	}

	@Override
	public String toString() {

		String info = "";
		if (customer == null)
			info = "NONE";
		else
			info = getCustomer().getEmail();

		String info1 = "";
		if (packer == null)
			info1 = "NONE";
		else
			info1 = packer.getId();
		
		
		return "Order DATE:" + dateOfOrder + ", STATUS:" + status + ",  Customer:" + info +", Packer id:"+ info1;

	}

}
