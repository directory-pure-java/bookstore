package pjwstk.entity.actors;

import java.io.IOException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import pjwstk.data.AddressBookshelf;
import pjwstk.data.AddressCell;
import pjwstk.data.CompletationStatus;
import pjwstk.data.ContainerStatus;
import pjwstk.data.EmployeePosition;
import pjwstk.data.ObjectPlus;
import pjwstk.data.OrderStatus;
import pjwstk.entity.Bookshelf;
import pjwstk.entity.Bookshelf.Cell;
import pjwstk.entity.Container;
import pjwstk.entity.Order;
import pjwstk.entity.OrderDetails;

public class Packer extends Position {

	private static int norm;
	private Container container;
	private List<Order> orders = new ArrayList<>();
	/**
	 * 
	 */

	private static final long serialVersionUID = 1L;

	public Packer(String id, String name, String surname, List<String> phones) throws Exception {
		super(id, name, surname, phones);
		this.employeePosition = EmployeePosition.PACKER;
	}

	public static void createPacker(Employee employee) throws Exception {

		employee.changePosition(EmployeePosition.PACKER);

	}

	// 1-*
	public void addOrder(Order newOrder) throws Exception {
		// Check if we already have the info
		if (!orders.contains(newOrder)) {
			orders.add(newOrder);
			// Add the reverse connection
			newOrder.setPacker(this);
		}
	}

	public Optional<Container> takeContainer(int containerId, Order order) throws Exception {

		Optional<Container> tmpContainer;

		tmpContainer = Container.findContainer(containerId, ContainerStatus.EMPTY);

		if (tmpContainer.equals(Optional.empty())) {
			System.out.println("LOG: Containder id:" + containerId + " cant be taken.");
			return Optional.empty();
		} else {
			this.container = tmpContainer.get();
			this.container.setPacker(this);
			this.container.setStatus(ContainerStatus.NOTEMPTY);
			this.container.setOrder(order);

			System.out.println("LOG: Containder id:" + containerId + " was taken successful by Empplyee ID:"
					+ tmpContainer.get().getPacker().getId());

			System.out.println("LOG: Order container added");
			ObjectPlus.writeExtents();

			return tmpContainer;
		}
	}

	public Order takeOrder() throws Exception {

		String info = "";

		Optional<Order> order = Order.getFirstOrder();

		if (order.equals(Optional.empty())) {
			info = "LOG: Order cant be take.";
			System.out.println(info);
			return null;
		} else {
			order.get().setPacker(this);
			info = "LOG: " + order.get() + " was taken successful.";
			ObjectPlus.writeExtents();
			System.out.println(info);
			return order.get();
		}

	}

	public Container getContainer() {
		return container;
	}

	public static int getNorm() throws ClassNotFoundException, IOException {

		return norm;
	}

	public static void setNorm(Integer norm) throws Exception {

		if (norm == null)
			throw new Exception("Err: Norm   is required");

		Packer.norm = norm;
		ObjectPlus.writeExtents();
	}

	public List<Order> getOrders() {
		return orders;
	}

	public Optional<OrderDetails> getCurentPositionOrderDetails(List<OrderDetails> tmpOrderDetails) {

		Optional<OrderDetails> currentOrderDetails = tmpOrderDetails.stream()
				.filter(val -> CompletationStatus.NO.equals(val.getStatus())).findFirst();

		return currentOrderDetails;
	}

	public void skipBookInOrder(OrderDetails orderDetails) throws Exception {

		orderDetails.setStatus(CompletationStatus.NON);
		OrderDetails.changeStatus(orderDetails, CompletationStatus.NON);
		System.out.println("LOG: book was skiped in order.");

	}

	public boolean putAsaidContainer(AddressCell addressCell, Container container) throws Exception {

		Optional<Cell> cell = Bookshelf.findCell(new AddressBookshelf(0, 0, 0), addressCell);

		if (cell.equals(Optional.empty())) {
			System.out.println("LOG: wrong cell address.");
			return false;
		} else {
			cell.get().addContainer(container);

			Order tmp = Order.find(container.getOrder().getCustomer().getEmail(), OrderStatus.INPOGRESS);
			tmp.setStatus(OrderStatus.PUTDOWN);
			ObjectPlus.writeExtents();
			System.out.println("LOG: Container has been set aside.");

			return true;
		}

	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}

	@Override
	public String showInfo() {

		String info = this.getClass().toString().replace("class pjwstk.entity.", "") + ":" + getId() + " status:"
				+ getStatus();
		if (!(this.getScanner() == null))
			info += " scanner id:";

		return info;

	}

	@Override
	public String toString() {
		return "ID:" + getId() + " " + getName() + " " + getSurname() + ", Positon:" + getEmployeePosition()
				+ ", Status:" + getStatus();
	}
}
