package pjwstk.entity.actors;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import java.util.List;
import java.util.Optional;
import pjwstk.data.ObjectPlus;
import pjwstk.data.OrderStatus;
import pjwstk.entity.Book;
import pjwstk.entity.Order;
import pjwstk.entity.OrderDetails;

public class Customer extends User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String email;
	private List<Order> orders = new ArrayList<Order>();

	public Customer(String name, String surname, String email) throws Exception {
		super(name, surname);

		setEmail(email);

	}

	@SuppressWarnings("unused")
	public static void createCustomer(String name, String surname, String email) throws Exception {
		ObjectPlus.readExtents();
		Customer customer = new Customer(name, surname, email);
		ObjectPlus.writeExtents();
	}

	public static Optional<Customer> find(String email) throws ClassNotFoundException, IOException {

		ObjectPlus.readExtents();

		Iterable<Customer> source = ObjectPlus.getExtent(Customer.class);
		List<Customer> target = new ArrayList<>();
		source.forEach(target::add);

		return target.stream().filter(v -> v.getEmail().equals(email)).findFirst();

	}

	public void makeOrder() throws Exception {

		Order o = Order.createOrder();
		o.setStatus(OrderStatus.NONEBOOK);
		addOrder(o);

		System.out.println(
				"LOG: Customer:" + getName() + " " + getSurname() + " created new Order: " + o.getDateOfOrder());
	}

	public void createOrderDetails(String ISBN, int quantity) throws Exception {

		Book book = Book.find(ISBN).get();
		Order order = Order.find(this.email, OrderStatus.NONEBOOK);

		OrderDetails.createOrderDetails(book, order, quantity);

		System.out.println(
				"LOG: Orderdetails  DATE:" + order.getDateOfOrder() + ", customer:" + order.getCustomer().getEmail()
						+ ", ISBN:" + book.getISBN() + ", stack:" + quantity + " was created.");
		ObjectPlus.writeExtents();
	}

	// 1-*
	public void addOrder(Order newOrder) throws Exception {
		newOrder.setStatus(OrderStatus.NONEBOOK);
		// Check if we already have the info
		if (!orders.contains(newOrder)) {
			orders.add(newOrder);
			// Add the reverse connection
			newOrder.setCustomer(this);
		}

	}

	public List<Order> getOrders() {
		return orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) throws Exception {

		if (email == null)
			throw new Exception("Err: Email title is required.");

		this.email = email;
	}

	@Override
	public String showInfo() {
		String info = this.getClass().toString().replace("class pjwstk.entity.actors.", "") +": "+ getName() + " " + getSurname() +" " + getEmail() ;
		return info;
	}

	@Override
	public String toString() {

		String tmpEmail;

		if (getEmail() == null)
			tmpEmail = "none";
		else
			tmpEmail = getEmail();

		return "Customer [" + getName() + " " + getSurname() + ", email: " + tmpEmail + "]";
	}

}
