package pjwstk.entity.actors;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.security.sasl.AuthorizeCallback;

import pjwstk.data.AddressBookshelf;
import pjwstk.data.AddressCell;
import pjwstk.data.EmployeePosition;
import pjwstk.data.ObjectPlus;
import pjwstk.entity.Author;
import pjwstk.entity.Book;
import pjwstk.entity.Bookshelf;
import pjwstk.entity.Container;
import pjwstk.entity.Scanner;
import pjwstk.entity.Bookshelf.Cell;
import pjwstk.interfaces.IStorekeeper;

public class Manager extends Packer implements IStorekeeper {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Manager(String id, String name, String surname, List<String> phones) throws Exception {
		super(id, name, surname, phones);
		setEmployeePosition(employeePosition.MANAGER);
	}

	@Override
	public void createNewAuthor(String name, String surname, int bYear) throws Exception {

		Author.createAuthor(name, surname, bYear);
		Optional<Author> author = Author.findAuthor(name, surname);

		if (author.equals(Optional.empty()))
			throw new Exception("Err: with author  data.");

		System.out.println("LOG: Author " + author.get() + " was created.");

	}

	@Override
	public void createNewBook(String ISBN, String title, int stack, String name, String surname) throws Exception {

		Book.createBook(ISBN, title, stack);
		Author a = Author.findAuthor(name, surname).get();
		if (a.equals(Optional.empty()))
			throw new Exception("Err: with author.");
		Book b = Book.find(ISBN).get();
		if (b.equals(Optional.empty()))
			throw new Exception("Err: with book data.");

		a.addBookQualif(b);
		System.out.println("LOG: add Author-Book " + a + "-" + b);
	}

	@Override
	public void createNewBookshelf(AddressBookshelf addressBookshelf) throws Exception {

		Bookshelf.createBookshelf(addressBookshelf);
		Optional<Bookshelf> bookshelf = Bookshelf.findBookshelf(addressBookshelf);

		if (bookshelf.equals(Optional.empty()))
			throw new Exception("Err: with Bookshelf data.");

		System.out.println("LOG: " + bookshelf.get() + " was created.");

	}

	@Override
	public void createNewCell(AddressBookshelf ab, AddressCell ac) throws Exception {

		Optional<Bookshelf> bookshelf = Bookshelf.findBookshelf(ab);
		System.out.println("LOG: Cell:" + bookshelf.get().createCell(ac) + " added to:" + bookshelf.get());
		ObjectPlus.writeExtents();
	}

	@Override
	public void addBooksInCell(String ISBN, int stock) throws ClassNotFoundException, IOException {

System.out.println("DODAJE " + stock);
		
		Iterable<Book> source = ObjectPlus.getExtent(Book.class);
		List<Book> target = new ArrayList<>();
		source.forEach(target::add);

		Book book = target.stream().filter(v -> v.getISBN().equals(ISBN)).findFirst().get();
		book.getCell().setStock(stock);
		System.out.println("LOG: ISBN:" + ISBN + " added " + stock + " itmes to Cell:"+ book.getCell());
	
	ObjectPlus.writeExtents();
	}

	public void createNewScanner(int id) throws Exception {

		Scanner.createScanner(id);
		Optional<Scanner> scanner = Scanner.findById(id);

		if (scanner.equals(Optional.empty()))
			throw new Exception("Err with created scanner");

		System.out.println("LOG: " + scanner.get() + " was created.");

	}

	public void createNewContainer(int id) throws Exception {

		Container.createContainer(id);
		Optional<Container> container = Container.findById(id);

		if (container.equals(Optional.empty()))
			throw new Exception("Err with created container");

		System.out.println("LOG: " + container.get() + " was created.");

	}

	public void createNewEmployee(String id, String name, String surname, List<String> phones) throws Exception {

		Employee.createEmployee(id, name, surname, phones);
		Optional<Employee> employee = Employee.findById(id);

		if (employee.equals(Optional.empty()))
			throw new Exception("Err with created employee");

		System.out.println("LOG: " + employee.get() + " was created.");
	}

	public void createNewCustomer(String name, String surname, String email) throws Exception {

		Customer.createCustomer(name, surname, email);

		Optional<Customer> customer = Customer.find(email);
		if (customer.equals(Optional.empty()))
			throw new Exception("Err with created customer.");

		System.out.println("LOG: " + customer.get() + " was created.");
	}

	public void setPackerNorm(int norm) throws Exception {

		Packer.setNorm(norm);

	}

	public void addCustomersPrivilages(String id, String email) throws Exception {


		Iterable<Employee> source = ObjectPlus.getExtent(Employee.class);
		List<Employee> target = new ArrayList<>();
		source.forEach(target::add);
		Employee employee = target.stream().filter(v -> v.getId().equals(id)).findFirst().get();
		if (employee.getCustomer() == null) {
			employee.setCustomer(employee.getName(), employee.getSurname(), email);
		}
		ObjectPlus.writeExtents();

	}

	public void setJobPosition(String id, EmployeePosition employeePosition) throws Exception {

		Iterable<Employee> source = ObjectPlus.getExtent(Employee.class);
		List<Employee> target = new ArrayList<>();
		source.forEach(target::add);

		Employee employee = target.stream().filter(v -> v.getId().equals(id)).findFirst().get();

		switch (employeePosition) {

		case PACKER:

			Packer.createPacker(employee);

			break;

		case STOREKEEPER:

			Storekeeper.createStorekeeper(employee);

			break;
		default:
			break;
		}

		System.out.println("LOG: " + employee + " received new privileges.");

	}

	@Override
	public void setBookInCell(String ISBN, AddressBookshelf ab, AddressCell ac)
			throws ClassNotFoundException, IOException {

		Optional<Cell> cell = Bookshelf.findCell(ab, ac);
		Optional<Book> book = Book.find(ISBN);

		book.get().setCell(cell.get());
	
		System.out.println("LOG: Book:" + book.get().getISBN() + " added to Cell:" + cell.get());

	}

	@Override
	public String toString() {
		return "Manager ID=" + getId() + " " + getName() + " " + getSurname() + ": " + employeePosition;
	}

}
