package pjwstk.entity.actors;

import java.util.List;

import pjwstk.entity.Person;

public class User extends Person {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Employee employee;
	private Customer customer;

	public User(String name, String surname) throws Exception {
		super(name, surname);
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(String id, String name, String surname, List<String> phones) throws Exception {
		this.employee = new Employee(id, name, surname, phones);
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(String name, String surname, String email) throws Exception {

		this.customer = new Customer(name, surname, email);
	}

	public String showInfo() {

		String info = this.getClass().toString().replace("class pjwstk.entity.", "") + ": " + getName() + " "
				+ getSurname();
		return info;

	};

	@Override
	public String toString() {

		String tmpEmployee = null;
		String tmpCustomer = null;

		if (this.employee == null)
			tmpEmployee = "NO";
		else
			tmpEmployee = "YES";
		if (this.customer == null)
			tmpCustomer = "NO";
		else
			tmpCustomer = "YES";

		String info = "User: " + this.getName() + " " + getSurname() + ", Employee=" + tmpEmployee + " Customer="
				+ tmpCustomer;
		return info;
	};

}
