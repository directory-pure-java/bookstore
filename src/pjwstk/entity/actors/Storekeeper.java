package pjwstk.entity.actors;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import pjwstk.data.AddressBookshelf;
import pjwstk.data.AddressCell;
import pjwstk.data.EmployeePosition;
import pjwstk.data.ObjectPlus;
import pjwstk.entity.Author;
import pjwstk.entity.Book;
import pjwstk.entity.Bookshelf;
import pjwstk.entity.Bookshelf.Cell;
import pjwstk.interfaces.IStorekeeper;

public class Storekeeper extends Position implements IStorekeeper {

	public static String norm;
	private List<Cell> cells = new ArrayList<Cell>();

	/**
	 * 
	 * 
	 */
	private static final long serialVersionUID = 1L;

	Storekeeper(String id, String name, String surname, List<String> phones) throws Exception {
		super(id, name, surname, phones);
	}

	public static void createStorekeeper(Employee employee) throws Exception {
		employee.changePosition(EmployeePosition.STOREKEEPER);

	}

	@Override
	public void createNewAuthor(String name, String surname, int bYear) throws Exception {

		Author.createAuthor(name, surname, bYear);
		Optional<Author> author = Author.findAuthor(name, surname);

		if (author.equals(Optional.empty()))
			throw new Exception("Err: with author  data.");

		System.out.println("LOG: Author " + author.get() + " was created.");

	}

	@Override
	public void createNewBook(String ISBN, String title, int stack, String name, String surname) throws Exception {

		Book.createBook(ISBN, title, stack);
		Author a = Author.findAuthor(name, surname).get();
		if (a.equals(Optional.empty()))
			throw new Exception("Err: with author.");
		Book b = Book.find(ISBN).get();
		if (b.equals(Optional.empty()))
			throw new Exception("Err: with book data.");

		a.addBookQualif(b);
		System.out.println("LOG: add Author-Book " + a + "-" + b);
	}

	@Override
	public void createNewBookshelf(AddressBookshelf addressBookshelf) throws Exception {

		Bookshelf.createBookshelf(addressBookshelf);
		Optional<Bookshelf> bookshelf = Bookshelf.findBookshelf(addressBookshelf);

		if (bookshelf.equals(Optional.empty()))
			throw new Exception("Err: with Bookshelf data.");

		System.out.println("LOG: " + bookshelf.get() + " was created.");

	}

	@Override
	public void createNewCell(AddressBookshelf ab, AddressCell ac) throws Exception {

		Optional<Bookshelf> bookshelf = Bookshelf.findBookshelf(ab);
		System.out.println("LOG: Cell:" + bookshelf.get().createCell(ac) + " added to:" + bookshelf.get());
		ObjectPlus.writeExtents();
	}

	@Override
	public void addBooksInCell(String ISBN, int stock) throws ClassNotFoundException, IOException {

		ObjectPlus.readExtents();
		Iterable<Book> source = ObjectPlus.getExtent(Book.class);
		List<Book> target = new ArrayList<>();
		source.forEach(target::add);

		Book book = target.stream().filter(v -> v.getISBN().equals(ISBN)).findFirst().get();
		book.getCell().setStock(stock);

	}

	@Override
	public void setBookInCell(String ISBN, AddressBookshelf ab, AddressCell ac)
			throws ClassNotFoundException, IOException {

		Optional<Cell> cell = Bookshelf.findCell(ab, ac);
		Optional<Book> book = Book.find(ISBN);

		book.get().setCell(cell.get());
		ObjectPlus.writeExtents();
		System.out.println("LOG: Book:" + book.get().getISBN() + " added to Cell:" + cell.get());

	}

	@Override
	public String toString() {
		return "Storekeepr";
	}

}
