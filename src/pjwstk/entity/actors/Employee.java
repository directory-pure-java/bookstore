package pjwstk.entity.actors;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import pjwstk.data.EmployeePosition;
import pjwstk.data.EmployeeStatus;
import pjwstk.data.ObjectPlus;
import pjwstk.data.ScannerStatus;
import pjwstk.entity.Scanner;
import pjwstk.entity.Bookshelf.Cell;

public class Employee extends User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;
	private List<String> phones = new ArrayList<String>();
	private Position position;
	private Scanner scanner;
	private String password;
	private EmployeeStatus status = EmployeeStatus.LOGOUT;

	public Employee(String id, String name, String surname, List<String> phones) throws Exception {
		super(name, surname);

		setPhones(phones);

		this.id = id;
		this.password = this.id;

	}

	public static Optional<Employee> findById(String id) throws ClassNotFoundException, IOException {

		Iterable<Employee> source = ObjectPlus.getExtent(Employee.class);
		List<Employee> target = new ArrayList<>();
		source.forEach(target::add);

		return target.stream().filter(v -> v.getId().equals(id)).findFirst();

	}

	public static String createEmployee(String id, String name, String surname, List<String> phones) throws Exception {

		Employee employee = new Employee(id, name, surname, phones);
		ObjectPlus.writeExtents();
		return employee.getId();
	}

	// 1-1
	public void setScanner(Scanner newScanner) throws Exception {
		if (newScanner == null) {
			this.scanner = null;
			
		} else if (this.scanner == null) {
			this.scanner = newScanner;
			this.scanner.setEmployee(this);

		} else if (!this.scanner.equals(newScanner)) {
			this.scanner.setEmployee(null);
			this.scanner = newScanner;
			this.scanner.setEmployee(this);

		}
		
	}

	public void changePosition(EmployeePosition status) throws Exception {

		if (status.equals(EmployeePosition.PACKER)) {
			this.position = new Packer(getId(), getName(), getSurname(), getPhones());
			this.position.employeePosition = status;
		} else if (status.equals(EmployeePosition.STOREKEEPER))

		{
			this.position = new Storekeeper(getId(), getName(), getSurname(), getPhones());
			this.position.employeePosition = status;
		}

		ObjectPlus.writeExtents();
	}

	public static Optional<Packer> login(String login, String password, int scannerId) throws Exception {
		ObjectPlus.readExtents();
		String info = "";

		Optional<Employee> tmpEmployee = Employee.findById(login);
		if (tmpEmployee.equals(Optional.empty()))
			return Optional.empty();

		Packer p = (Packer) tmpEmployee.get().getPosition();
		
		Optional<Scanner> tmpScanner = Scanner.findById(scannerId);

		if (!tmpEmployee.equals(Optional.empty()) && !tmpScanner.equals(Optional.empty()))

		{
			if (tmpEmployee.get().getPassword().equals(password)) {
				// password ok

				p.setScanner(tmpScanner.get());

				tmpScanner.get().setStatus(ScannerStatus.BUSY);
				p.setStatus(EmployeeStatus.LOGIN);
				tmpEmployee.get().setStatus(EmployeeStatus.LOGIN);

				ObjectPlus.writeExtents();

				info = "LOG: Employee ID=" + tmpEmployee.get().getId() + " " + tmpEmployee.get().getStatus()
						+ " on scanner ID=" + tmpScanner.get().getId();
				System.out.println(info);

			} else {
				p = null;
				info = "LOG: login is not possible!";
				System.out.println(info);
			}
		} else {
			p = null;
			info = "LOG: login is not possible!";
			System.out.println(info);
		}

		if (p == null)
			return Optional.empty();
		else

			return Optional.of(p);

	}

	public void logout(Packer packer) throws Exception {
		String tmpId = getId();
		
		System.out.println("Wylogowuje");
		
		getScanner().setStatus(ScannerStatus.FREE);
		packer.setStatus(EmployeeStatus.LOGOUT);
		packer.getScanner().setEmployee(null);
		packer.setScanner(null);

		ObjectPlus.writeExtents();

		System.out.println("LOG: logout ID:" + tmpId);

	}

	public List<String> getPhones() {

		return phones;
	}

	public void setPhones(List<String> phones) {
		if (phones == null || phones.size() == 0)
			this.phones.add(new String("none"));
		else
			this.phones = phones;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public EmployeeStatus getStatus() {
		return status;
	}

	public void setStatus(EmployeeStatus status) throws IOException, ClassNotFoundException {

		this.status = status;
		ObjectPlus.writeExtents();
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public Scanner getScanner() {
		return scanner;
	}

	@Override
	public String showInfo() {
		String info = this.getClass().toString().replace("class pjwstk.entity.actors", "") + ": " + getName() + " "
				+ getSurname();
		return info;
	}

	@Override
	public String toString() {

		String tmp = "";
		if (position == null)
			tmp = "none";
		else
			tmp = position.getClass().toString().replace("class pjwstk.entity.actors.", "");

		String info = "ID=" + getId() + ": " + getName() + " " + getSurname() + " position: " + tmp + ", Status:"
				+ status;

		return info;
	}

}
