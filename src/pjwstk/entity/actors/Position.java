package pjwstk.entity.actors;

import java.util.List;

import pjwstk.data.EmployeePosition;

public abstract class Position extends Employee {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected EmployeePosition employeePosition;

	protected Position(String id, String name, String surname, List<String> phones) throws Exception {
		super(id, name, surname, phones);
		this.employeePosition = EmployeePosition.NONE;
	}

		
	
	public EmployeePosition getEmployeePosition() {
		return employeePosition;
	}

	public void setEmployeePosition(EmployeePosition employeePosition) {
		this.employeePosition = employeePosition;
	}

}
