package pjwstk.entity;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import pjwstk.data.CompletationStatus;
import pjwstk.data.ObjectPlus;
import pjwstk.data.OrderStatus;

public class OrderDetails extends ObjectPlus implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Book book;
	private Order order;
	private int quantity;
	private CompletationStatus status = CompletationStatus.NO;

	private OrderDetails(Book book, Order order, Integer quantity) throws Exception {
		super();

		setBook(book);
		setOrder(order);
		setQuantity(quantity);

	}

	@SuppressWarnings("unused")
	public static void createOrderDetails(Book book, Order order, int quantity) throws Exception {

		OrderDetails orderDetails = new OrderDetails(book, order, quantity);
		ObjectPlus.writeExtents();
	}

	public void setBook(Book newBook) throws Exception {

		if (newBook == null)
			throw new Exception("Err: Book  is required.");

		if (this.book != null) {

			this.book.getOrderDetails().remove(this);
			this.book = newBook;
			newBook.getOrderDetails().add(this);
		} else
			this.book = newBook;
		newBook.getOrderDetails().add(this);
	}

	public void setOrder(Order newOrder) throws Exception {

		newOrder.setStatus(OrderStatus.PENDING);

		if (newOrder == null)
			throw new Exception("Err: Order  is required.");

		if (this.order != null) {

			this.order.getOrderDetails().remove(this);
			this.order = newOrder;
			this.order.getOrderDetails().add(this);
		} else
			this.order = newOrder;
		this.order.getOrderDetails().add(this);
	}

	public static void changeStatus(OrderDetails orderDetails, CompletationStatus completationStatus)
			throws ClassNotFoundException, IOException {

		
		Iterable<OrderDetails> source = ObjectPlus.getExtent(OrderDetails.class);

		Optional<OrderDetails> tmpOrderdetails;

		List<OrderDetails> target = new ArrayList<>();
		source.forEach(target::add);

		tmpOrderdetails = target.stream()
				.filter(v -> v.order.getDateOfOrder().equals(orderDetails.getOrder().getDateOfOrder())
						&& v.book.getISBN().equals(orderDetails.getBook().getISBN()))
				.findFirst();

		tmpOrderdetails.get().status = completationStatus;

		ObjectPlus.writeExtents();

	}

	public static List<OrderDetails> getOrdersDetails(Order o) throws Exception {
		if (o != null) {
		
			Iterable<OrderDetails> source = null;
			source = ObjectPlus.getExtent(OrderDetails.class);

			List<OrderDetails> target = new ArrayList<>();
			source.forEach(target::add);

			return target.stream().filter(v -> v.order.getDateOfOrder().equals(o.getDateOfOrder()))
					.collect(Collectors.toCollection(ArrayList::new));
		} else {
			throw new Exception("Err: ");
		}
	}

	public Order getOrder() {
		return order;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) throws Exception {
		if (quantity == null)
			throw new Exception("Err: Quantity in orderdetails is required.");
		this.quantity = quantity;
	}

	public CompletationStatus getStatus() {
		return status;
	}

	public void setStatus(CompletationStatus status) throws Exception {
	
		if (status == null)
			throw new Exception("Err: Orderdetails status is required.");
		this.status = status;
		ObjectPlus.writeExtents();
	}

	public Book getBook() {

		return book;
	}

	@Override
	public String toString() {
		return "OrderDetails [" + book.getISBN() + ", quantity=" + quantity + ", status=" + status + " Cell:"
				+ book.getCell() + " customer=" + order.getCustomer().getEmail() + "]";
	}

}
