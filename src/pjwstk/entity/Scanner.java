package pjwstk.entity;


import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import pjwstk.data.EmployeeStatus;
import pjwstk.data.ObjectPlus;
import pjwstk.data.ScannerStatus;
import pjwstk.entity.actors.Employee;

public class Scanner extends ObjectPlus implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private ScannerStatus status;
	private Employee employee;

	private Scanner(Integer id) throws Exception {
		super();
		setId(id);
		setStatus(ScannerStatus.FREE);

	}

	@SuppressWarnings("unused")
	public static void createScanner(int id) throws Exception {


		Scanner scanner = new Scanner(id);
		ObjectPlus.writeExtents();

	}

	public int getId() {
		return id;
	}

	public void setId(Integer id) throws Exception {
		if (id == null)
			throw new Exception("Err: Scanner id  is required.");
		this.id = id;
	}

	public ScannerStatus getStatus() {
		return status;
	}

	public void setStatus(ScannerStatus status) throws Exception {

		if (status == null)
			throw new Exception("Err: Scanner status is required.");
		this.status = status;
	}

	public Employee getEmployee() {
		return employee;
	}

	public static Optional<Scanner> findById(int id) throws ClassNotFoundException, IOException {


		Iterable<Scanner> source = ObjectPlus.getExtent(Scanner.class);
		List<Scanner> target = new ArrayList<>();
		source.forEach(target::add);

		return target.stream().filter(v -> v.getId() == id).findFirst();

	}

	public void setEmployee(Employee newEmployee) throws Exception {

		if (newEmployee == null) {
			this.employee = null;

		}

		else if (this.employee == null) {
			this.employee = newEmployee;
			this.employee.setStatus(EmployeeStatus.LOGIN);
			this.employee.setScanner(this);
		} else if (!this.employee.equals(newEmployee)) {
			this.employee.setScanner(null);
			this.employee = newEmployee;
			this.employee.setScanner(this);
		}

	}

	@Override
	public String toString() {

		String tmpEmploye;
		if (this.employee == null)
			tmpEmploye = "none";
		else
			tmpEmploye = getEmployee().getId();

		return "Scanner [id:" + id + ", status:" + status + ", employee ID:" + tmpEmploye + "]";
	}

}
