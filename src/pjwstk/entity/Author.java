package pjwstk.entity;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

import pjwstk.data.ContainerStatus;
import pjwstk.data.ObjectPlus;
import pjwstk.entity.actors.User;

public class Author extends Person implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int bYear;
	private Map<String, Book> booksQualif;

	public Author(String name, String surname, Integer bYear) throws Exception {
		super(name, surname);

		setbYear(bYear);
		booksQualif = new TreeMap<>();
	}

	public static void createAuthor(String name, String surname, int bYear) throws Exception {

	
		Author author = new Author(name, surname, bYear);
		ObjectPlus.writeExtents();

	}

	public static Optional<Author> findAuthor(String name, String surname) throws ClassNotFoundException, IOException {

		Optional<Author> tmpAuthor;


		Iterable<Author> source = ObjectPlus.getExtent(Author.class);
		List<Author> target = new ArrayList<>();
		source.forEach(target::add);

		tmpAuthor = target.stream().filter(v -> v.getName().equals(name) && v.getSurname().equals(surname)).findFirst();

		return tmpAuthor;
	}

	// kwalfikowana
	public void addBookQualif(Book newBook) throws Exception {

		if (!booksQualif.containsKey(newBook.getISBN())) {
			this.booksQualif.put(newBook.getISBN(), newBook);
			newBook.addAuthor(this);

		}
		ObjectPlus.writeExtents();
	}

	public Book findBookQualif(String ISBN) throws Exception {
		// Check if we have the info
		if (!booksQualif.containsKey(ISBN)) {
			throw new Exception("Unable to find a book: " + ISBN);
		}

		return booksQualif.get(ISBN);
	}

	public Map<String, Book> getBooksQualif() {
		return booksQualif;
	}

	public void setBooksQualif(Map<String, Book> booksQualif) {
		this.booksQualif = booksQualif;
	}

	public int getbYear() {
		return bYear;
	}

	public void setbYear(Integer bYear) throws Exception {
		if (bYear == null)
			throw new Exception("Err: Born year  is required");
		this.bYear = bYear;
		ObjectPlus.writeExtents();
	}

	public void setbYear(int bYear) {
		this.bYear = bYear;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return getName() + " " + getSurname() + " " + getbYear();
	}

	@Override
	public String showInfo() {

		return "Author: "+getName() +" " + getSurname() +" "+ getbYear();

		
	}

}
