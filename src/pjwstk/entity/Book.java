package pjwstk.entity;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import pjwstk.data.ObjectPlus;
import pjwstk.entity.Bookshelf.Cell;

public class Book extends ObjectPlus implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String ISBN;
	private String title;
	private int stock;

	private List<Author> authors;
	private List<OrderDetails> orderDetails = new ArrayList<>();
	private Cell cell;

	private Book(String ISBN, String title, Integer stock) throws Exception {
		super();
		this.authors = new ArrayList<>();
		setISBN(ISBN);
		setTitle(title);
		setStock(stock);

	}

	public static void createBook(String ISBN, String title, int stack) throws Exception {

		Book book = new Book(ISBN, title, stack);

		ObjectPlus.writeExtents();
		System.out.println("LOG: Book" + book + " was created.");

	}

	public void addOrderDetails(OrderDetails newOrderDetail) throws Exception {
		// Check if we already have the info
		if (!orderDetails.contains(newOrderDetail)) {
			orderDetails.add(newOrderDetail);
			// Add the reverse connection
			newOrderDetail.setBook(this);
		}
	}

	public void addAuthor(Author newAuthor) throws Exception {
		// Check if we have the information already
		if (!authors.contains(newAuthor)) {
			authors.add(newAuthor);
			// Add the reverse connection
			newAuthor.addBookQualif(this);
		}
	}

	public void setCell(Cell newCell) throws IOException, ClassNotFoundException {

		if (this.cell == null) {
			this.cell = newCell;
			this.cell.setBook(this);

		} else if (this.cell != newCell) {
			this.cell.getBook().setCell(null);
			this.cell = newCell;
			this.cell.setBook(this);
		}
		ObjectPlus.writeExtents();
	}

	public Cell getCell() {
		return this.cell;
	}

	public String getAuthors() {
		String info = "";

		for (Author author : authors) {
			info += author.getName().charAt(0) + "." + author.getSurname() + " ";
		}

		return info;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) throws Exception {
		if (title == null)
			throw new Exception("Err: Book title is required.");
		this.title = title;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(Integer stock) throws Exception {

		if (stock == null)
			throw new Exception("Err: Stock is required.");
		this.stock = stock;
	}

	public String getISBN() {
		return ISBN;
	}

	public void setISBN(String ISBN) throws Exception {
		if (ISBN == null)
			throw new Exception("Err: ISBN is required.");
		this.ISBN = ISBN;
	}

	public List<OrderDetails> getOrderDetails() {
		return orderDetails;
	}

	public void setOrderDetails(List<OrderDetails> orderDetails) {
		this.orderDetails = orderDetails;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public static Optional<Book> find(String ISBN) throws ClassNotFoundException, IOException {
		Optional<Book> tmpBook;

		Iterable<Book> source = ObjectPlus.getExtent(Book.class);
		List<Book> target = new ArrayList<>();
		source.forEach(target::add);

		tmpBook = target.stream().filter(v -> v.getISBN().equals(ISBN)).findFirst();

		return tmpBook;

	}

	public void setAuthors(List<Author> authors) {
		this.authors = authors;
	}

	@Override
	public String toString() {
		return "[Title:" + getTitle() + ", author:" + this.authors + ", stosck:" + getStock() + "]";
	}

}
