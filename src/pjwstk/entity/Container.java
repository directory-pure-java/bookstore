package pjwstk.entity;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import pjwstk.data.ContainerStatus;
import pjwstk.data.ObjectPlus;
import pjwstk.entity.Bookshelf.Cell;
import pjwstk.entity.actors.Packer;

public class Container extends ObjectPlus implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int id;
	private ContainerStatus status;

	private Order order;
	private Packer packer;
	private Cell cell;

	private Container(Integer id) throws Exception {
		super();

		setId(id);
		setStatus(ContainerStatus.EMPTY);
	}

	public static void createContainer(int id) throws Exception {

		Container container = new Container(id);
		ObjectPlus.writeExtents();

	}

	public static Optional<Container> findById(int id) throws ClassNotFoundException, IOException {
	;
		Iterable<Container> source = ObjectPlus.getExtent(Container.class);
		List<Container> target = new ArrayList<>();
		source.forEach(target::add);

		return target.stream().filter(v -> v.getId() == id).findFirst();
	}

	// 1-1
	public void setOrder(Order newOrder) {

		if (this.order == null) {
			this.order = newOrder;
			this.order.setContainer(this);
		} else if (!this.order.equals(newOrder)) {
			this.order.setContainer(null);
			this.order = newOrder;
			this.order.setContainer(this);
		}

	}

	// 1-*
	public void setCell(Cell newCell) throws Exception {

		if (this.cell == null) {

			this.cell = newCell;
			this.cell.addContainer(this);
		}

		else {

			this.cell.getContainers().remove(this);
			this.cell = newCell;
			newCell.addContainer(this);
		}
	}

	public int getId() {
		return id;
	}

	public void setId(Integer id) throws Exception {

		if (id == null)
			throw new Exception("Err: Id is required");
		this.id = id;
	}

	public ContainerStatus getStatus() {
		return status;
	}

	public void setStatus(ContainerStatus status) throws Exception {
		if (status == null)
			throw new Exception("Err: Container is required");

		this.status = status;
	}

	public static Optional<Container> findContainer(int containerId, ContainerStatus statuss)
			throws ClassNotFoundException, IOException {
		Optional<Container> tmpContainer;
		

		Iterable<Container> source = ObjectPlus.getExtent(Container.class);
		List<Container> target = new ArrayList<>();
		source.forEach(target::add);

		try {
			tmpContainer = target.stream().filter(v -> v.getStatus().equals(statuss) && v.getId() == (containerId))
					.findFirst();

			return tmpContainer;
		} catch (Exception e) {
			tmpContainer = null;
		}

		return tmpContainer;
	}

	public Order getOrder() {
		return order;
	}

	public Packer getPacker() {
		return packer;
	}

	public void setPacker(Packer packer) {
		this.packer = packer;
	}

	@Override
	public String toString() {
		String tmp ="none";
		if (order!=null)
			tmp=getOrder().toString();
		return "Container [id:" + id + ", status:" + status + ", Order:"+tmp+"]";
	}

}
