package pjwstk.entity;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import pjwstk.data.AddressBookshelf;
import pjwstk.data.AddressCell;
import pjwstk.data.ContainerStatus;
import pjwstk.data.ObjectPlus;
import pjwstk.entity.Bookshelf.Cell;

public class Bookshelf extends ObjectPlus implements Serializable {

	/**
	* 
	*/
	private static final long serialVersionUID = 1L;

	public AddressBookshelf addressBookshelf;
	private List<Cell> cells = new ArrayList<>();

	private Bookshelf(AddressBookshelf addressBookshelf) throws Exception {
		super();

		setAddressBookshelf(addressBookshelf);
	}

	public static void createBookshelf(AddressBookshelf addressBookShelf) throws Exception {

		ObjectPlus.readExtents();
		Bookshelf bookshelf = new Bookshelf(addressBookShelf);
		ObjectPlus.writeExtents();
	}

	public Cell createCell(AddressCell cellAddress) throws Exception {
		Cell part = new Cell(cellAddress);

		Optional<Cell> tmp = cells.stream().filter(v -> v.address.toString().equals(cellAddress.toString()))
				.findFirst();

		if (tmp.equals(Optional.empty()))

		{
			cells.add(part);
		} else
			throw new Exception("Err: Cell exists");
		return part;
	}

	public AddressBookshelf getAddressBookshelf() {

		return addressBookshelf;
	}

	public void setAddressBookshelf(AddressBookshelf addressBookshelf) throws Exception {
		if (addressBookshelf == null)
			throw new Exception("Err: Address of bookshelf  is required");
		this.addressBookshelf = addressBookshelf;
	}

	public static Optional<Bookshelf> findBookshelf(AddressBookshelf ab) throws ClassNotFoundException, IOException {

		ObjectPlus.readExtents();

		Iterable<Bookshelf> source = ObjectPlus.getExtent(Bookshelf.class);
		List<Bookshelf> target = new ArrayList<>();
		source.forEach(target::add);

		return target.stream().filter(v -> v.getAddress().toString().equals((ab).toString())).findFirst();

	}

	public static Optional<Cell> findCell(AddressBookshelf ab, AddressCell ac)
			throws ClassNotFoundException, IOException {

		Bookshelf bookshelf = Bookshelf.findBookshelf(ab).get();

		return bookshelf.getCells().stream().filter(v -> v.address.toString().equals((ac).toString())).findFirst();

	}

	public List<Cell> getCells() {
		return cells;
	}

	public void setCells(List<Cell> cells) {
		this.cells = cells;
	}

	public AddressBookshelf getAddress() {
		return addressBookshelf;
	}

	public void setAddress(AddressBookshelf address) {
		this.addressBookshelf = address;
	}

	@Override
	public String toString() {
		String info = "Bookshelf: " + addressBookshelf;

		return info;
	}

	public class Cell extends ObjectPlus {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private AddressCell address;

		private Book book;
		private Integer stock = 0;
		private List<Container> containers = new ArrayList<Container>();

		private Cell(AddressCell addressCell) throws FileNotFoundException, IOException {
			super();
			this.address = addressCell;
		}

		// 1-*
		public void addContainer(Container newConainer) throws Exception {
			// Check if we already have the info
			if (!containers.contains(newConainer)) {
				containers.add(newConainer);
				// Add the reverse connection
				newConainer.setCell(this);
			}
		}

		public void setBook(Book newBook) throws ClassNotFoundException, IOException {

			if (this.book == null) {
				this.book = newBook;
				this.book.setCell(this);
			} else if (this.book != newBook) {
				this.book.setCell(null);
				this.book = newBook;
				this.book.setCell(this);

			}

		}

		public Integer getStock() {
			return stock;
		}

		public Book getBook() {
			return book;
		}

		public void setStock(Integer stock) {
			this.stock = stock;
		}

		public void minusBook(int quantity) throws IOException {

			this.stock += (-quantity);
			this.book.setStock(this.book.getStock() - quantity);
			ObjectPlus.writeExtents();
		}

		public AddressCell getAddressCell() {
			return address;
		}

		public void setAddressCell(AddressCell addressCell) {
			this.address = addressCell;
		}

		public AddressCell getAddress() {
			return address;
		}

		public void setAddress(AddressCell address) {
			this.address = address;
		}

		public List<Container> getContainers() {

			return null;
		}

		public Cell getBookshelf() {

			return this;
		}

		public AddressBookshelf getAddressBookshelf() {

			return addressBookshelf;
		}

		@Override
		public String toString() {
			return getAddressBookshelf().toString() + "-" + address.toString() ;
		}

	}

}
