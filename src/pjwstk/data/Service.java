package pjwstk.data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.swing.JFrame;

import pjwstk.entity.Bookshelf;
import pjwstk.entity.Container;
import pjwstk.entity.Order;
import pjwstk.entity.OrderDetails;
import pjwstk.entity.Scanner;
import pjwstk.entity.actors.Employee;
import pjwstk.entity.actors.Packer;

public class Service {
	private Packer tmpPacker;
	private List<OrderDetails> tmpOrderDetails;
	private Container tmpContainer;
	private JFrame mainFrame;

	private OrderDetails currrentCompletingOrderDetailsElement;

	public Service(Packer tmpPacker, List<OrderDetails> tmpOrderDetails, Container tmpContainer, JFrame mainFrame) {
		super();
		this.tmpPacker = tmpPacker;
		this.tmpOrderDetails = tmpOrderDetails;
		this.tmpContainer = tmpContainer;
		this.mainFrame = mainFrame;
	}

	public Optional<Order> getOrder() throws Exception {

		Order tmp = tmpPacker.takeOrder();

		if (tmp == null)
			return Optional.empty();
		else
			return Optional.of(tmp);
	}

	public void getContainer(int containerId) throws Exception {

		Optional<Container> tmp = tmpPacker.takeContainer(containerId, tmpOrderDetails.get(0).getOrder());

		if (!tmp.equals(Optional.empty())) {
			tmpContainer = tmp.get();
		}

	}

	// 100 -> 100% completation
	// 1-> value ok
	// 2-> value too less
	// 3-> vale too high

	public int finishCompletingCurrentOrderDetailsElement(String requiredCopies) throws Exception {
		Optional<OrderDetails> newOrderDetails = Optional.empty();

		if (Integer.parseInt(requiredCopies) == (currrentCompletingOrderDetailsElement.getQuantity())) {
			OrderDetails.changeStatus(currrentCompletingOrderDetailsElement, CompletationStatus.OK);
			currrentCompletingOrderDetailsElement.setStatus(CompletationStatus.OK);

			currrentCompletingOrderDetailsElement.getBook().getCell()
					.minusBook(currrentCompletingOrderDetailsElement.getQuantity());

			newOrderDetails = getCurrentPositionOrderDetails();
			if (newOrderDetails.equals(Optional.empty())) {
				return 100;
			}

			return 1;
		}

		else if ((currrentCompletingOrderDetailsElement.getQuantity()) < Integer.parseInt(requiredCopies)) {

			return 2;
		}

		else if ((currrentCompletingOrderDetailsElement.getQuantity()) > Integer.parseInt(requiredCopies)) {

			return 3;
		}
		return -1;
	}

	// 10 -> go to frame 1
	// 60 -> go to frame 6

	public int skipBookInOrder() throws Exception {

		tmpPacker.skipBookInOrder(currrentCompletingOrderDetailsElement);
		Optional<OrderDetails> tmp = tmpPacker.getCurentPositionOrderDetails(tmpOrderDetails);

		if (tmp.equals(Optional.empty())) {
			ObjectPlus.writeExtents();
			return 60;
		} else {
			currrentCompletingOrderDetailsElement = tmp.get();
			return 10;
		}

	}

	public Optional<OrderDetails> getCurrentPositionOrderDetails() {

		Optional<OrderDetails> tmp = tmpPacker.getCurentPositionOrderDetails(tmpOrderDetails);

		if (!tmp.equals(Optional.empty()))
			currrentCompletingOrderDetailsElement = tmp.get();

		return tmp;
	}

	public int checkEnteredISBN(String ISBN) {

		if (ISBN.equals(""))
			return -1;
		else if (currrentCompletingOrderDetailsElement.getBook().getISBN().equals(ISBN))
			return 1;
		else
			return 0;
	}

	public int getCountCompletedOrderDetails() {

		return (int) this.tmpOrderDetails.stream().filter(
				v -> v.getStatus().equals(CompletationStatus.OK) || v.getStatus().equals(CompletationStatus.NON))
				.count();

	}

	public Optional<Employee> login(String login, String password, Integer scannerId) throws Exception {

		return Optional.of(Employee.login(login, password, scannerId).get());

	}

	public void logout() throws Exception {
		tmpPacker.logout(tmpPacker);
	}

	public boolean putAsaidContainer(AddressCell addressCell) throws Exception {
		return tmpPacker.putAsaidContainer(addressCell, tmpContainer);
	}

	public Packer getTmpPacker() {
		return tmpPacker;
	}

	public void setTmpPacker(Packer tmpPacker) {
		this.tmpPacker = tmpPacker;
	}

	public List<OrderDetails> getTmpOrderDetails() {
		return tmpOrderDetails;
	}

	public void setTmpOrderDetails(List<OrderDetails> tmpOrderDetails) {
		this.tmpOrderDetails = tmpOrderDetails;
	}

	public Container getTmpContainer() {
		return tmpContainer;
	}

	public void setTmpContainer(Container tmpContainer) {
		this.tmpContainer = tmpContainer;
	}

	public JFrame getMainFrame() {
		return mainFrame;
	}

	public void setMainFrame(JFrame mainFrame) {
		this.mainFrame = mainFrame;
	}

	public Service clean() {

		this.tmpContainer = null;
		this.tmpOrderDetails = new ArrayList<OrderDetails>();
		this.currrentCompletingOrderDetailsElement = null;
		return this;
	}

	@Override
	public String toString() {
		return "Service [tmpPacker=" + tmpPacker + ", tmpOrderDetails=" + tmpOrderDetails + ", container="
				+ tmpContainer + "]";
	}

}
