package pjwstk.data;

import java.io.Serializable;

public class AddressCell implements Serializable {

	/**
		 * 
		 */
	private static final long serialVersionUID = 1L;

	private int row;
	private int column;

	public AddressCell(Integer row, Integer column) throws Exception {
		super();

		setColumn(column);
		setRow(row);

	}

	public int getRow() {
		return row;
	}

	public void setRow(Integer row) throws Exception {
		if (row == null)
			throw new Exception("Err: Row  cant be null");
		this.row = row;
	}

	public int getColumn() {
		return column;
	}

	public void setColumn(Integer column) throws Exception {
		if (column == null)
			throw new Exception("Err: Column  cant be null");
		this.column = column;
	}

	@Override
	public String toString() {
		return row + "-" + column;
	}

}
