package pjwstk.data;

public enum EmployeePosition {

	PACKER, STOREKEEPER, NONE, MANAGER
}
