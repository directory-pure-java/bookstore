package pjwstk.data;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

public abstract class ObjectPlus implements Serializable {

	private static Map<Class, List<ObjectPlus>> allExtents = new Hashtable<>();
	private static String fileName = "d:/person.bin";

	public ObjectPlus() throws FileNotFoundException, IOException {

		List<ObjectPlus> extent = null;
		Class theClass = this.getClass();

		if (allExtents.containsKey(theClass)) {
			extent = allExtents.get(theClass);
		} else {
			extent = new ArrayList();
			allExtents.put(theClass, extent);
		}
		extent.add(this);

	}

	public static <T> Iterable<T> getExtent(Class<T> type) throws ClassNotFoundException {

		if (allExtents.containsKey(type)) {
			return (Iterable<T>) allExtents.get(type);
		}
		throw new ClassNotFoundException(String.format("%s. Stored extents: %s", type.toString(), allExtents.keySet()));

	}

	public static void writeExtents() throws IOException {
		ObjectOutputStream stream = new ObjectOutputStream(new FileOutputStream(fileName));
		stream.writeObject(allExtents);
	}

	public static void readExtents() throws IOException, ClassNotFoundException {
		ObjectInputStream stream = new ObjectInputStream(new FileInputStream(fileName));
		allExtents = (Hashtable) stream.readObject();
	}

	public static void showExtent(Class theClass) throws Exception {
		List<ObjectPlus> extent = null;
		if (allExtents.containsKey(theClass)) {
			extent = allExtents.get(theClass);
		} else {
			throw new Exception("Unknown class " + theClass);
		}
		System.out.println("Extent of the class: " + theClass.getSimpleName());

		for (Object obj : extent) {
			System.out.println(obj);
		}
	}
}
