package pjwstk.data;

import javax.swing.*;

public class Alert {
	private JFrame f;

	public Alert(JFrame f) {
		this.f = f;

	}

	public boolean cancelItemOfOrder() {

		int dialogButton = JOptionPane.YES_NO_OPTION;

		int dialogResult = JOptionPane.showConfirmDialog(f, "Cancel order item?", "Confirm", dialogButton);

		if (dialogResult == 0) {
			return true;
		} else
			return false;
	}
}
