package pjwstk.data;

import java.io.Serializable;

public class AddressBookshelf implements Serializable {

	/**
		 * 
		 */
	private static final long serialVersionUID = 1L;
	private int floor;
	private int row;
	private int column;

	public AddressBookshelf(Integer floor, Integer row, Integer column) {
		super();
		this.floor = floor;
		this.row = row;
		this.column = column;
	}


	public int getFloor() {
		return floor;
	}


	public void setFloor(int floor) {
		this.floor = floor;
	}


	public int getRow() {
		return row;
	}


	public void setRow(int row) {
		this.row = row;
	}


	public int getColumn() {
		return column;
	}


	public void setColumn(int column) {
		this.column = column;
	}


	@Override
	public String toString() {
		return floor + "-" + row + "-" + column;
	}

}
