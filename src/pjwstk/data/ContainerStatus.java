package pjwstk.data;

public enum ContainerStatus {

	EMPTY, NOTEMPTY, BROKEN
}
