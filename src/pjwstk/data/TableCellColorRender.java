package pjwstk.data;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

public class TableCellColorRender implements TableCellRenderer {
	private static final TableCellRenderer RENDERER = new DefaultTableCellRenderer();

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int row, int column) {

		Color color = null;

		Component c = RENDERER.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		c.setBackground(Color.WHITE);
		((JLabel) c).setHorizontalAlignment(JLabel.CENTER);
		if (column == 4) {

			Object result = table.getValueAt(row, column);

			if (result.toString().equals("OK"))

				c.setBackground(Color.GREEN);
			else if (result.toString().equals("NON"))
				c.setBackground(Color.RED);
			else
				c.setBackground(Color.WHITE);
		}

		return c;
	}

}
